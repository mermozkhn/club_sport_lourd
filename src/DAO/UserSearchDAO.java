package DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserSearchDAO {
    // URL de connexion à la base de données
    final String URL = "jdbc:mysql://localhost:3306/club_sport";
    // Identifiant de connexion à la base de données
    final String LOGIN = "root";
    // Mot de passe de connexion à la base de données
    final String PASS = "root";
    // Booléen indiquant l'état de connexion
    Boolean connected = false;
    // Objet Connection pour établir la connexion
    Connection conn = null;

    // Bloc statique pour charger le driver JDBC de MySQL
    static {
        try {
            // Chargement de la classe du driver MySQL
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    // Méthode pour obtenir les connexions de la base de données
    public List<String[]> getConnexions() {
        // Requête SQL pour sélectionner toutes les connexions
        String query = "SELECT * FROM connexion";
        // Appel de la méthode fetchData pour exécuter la requête et récupérer les données
        return fetchData(query);
    }

    // Méthode pour obtenir les recherches de la base de données
    public List<String[]> getRecherches() {
        // Requête SQL pour sélectionner toutes les recherches
        String query = "SELECT * FROM recherche";
        // Appel de la méthode fetchData pour exécuter la requête et récupérer les données
        return fetchData(query);
    }

    // Méthode privée pour exécuter une requête SQL et récupérer les données
    private List<String[]> fetchData(String query) {
        // Liste pour stocker les données récupérées
        List<String[]> data = new ArrayList<>();

        // Bloc try-with-resources pour gérer automatiquement les ressources
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASS);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            // Récupération des métadonnées de la table
            ResultSetMetaData metaData = resultSet.getMetaData();
            // Nombre de colonnes dans le résultat
            int columnCount = metaData.getColumnCount();

            // Tableau pour stocker les noms des colonnes
            String[] columnNames = new String[columnCount];
            for (int i = 1; i <= columnCount; i++) {
                // Récupération du nom de chaque colonne
                columnNames[i - 1] = metaData.getColumnName(i);
            }
            // Ajout des noms de colonnes à la liste des données
            data.add(columnNames);

            // Parcours des lignes du résultat de la requête
            while (resultSet.next()) {
                // Tableau pour stocker une ligne de données
                String[] row = new String[columnCount];
                for (int i = 1; i <= columnCount; i++) {
                    // Récupération de la valeur de chaque colonne pour la ligne actuelle
                    row[i - 1] = resultSet.getString(i);
                }
                // Ajout de la ligne à la liste des données
                data.add(row);
            }

        } catch (SQLException e) {
            // Affichage de la pile d'exécution en cas d'erreur SQL
            e.printStackTrace();
        }

        // Retour des données récupérées
        return data;
    }

    // Méthode pour vider les tables "recherche" et "connexion"
    public void deleteData() {
        // Requêtes SQL pour supprimer toutes les données des tables
        String deleteRecherchesQuery = "DELETE FROM recherche";
        String deleteConnexionsQuery = "DELETE FROM connexion";

        // Bloc try-with-resources pour gérer automatiquement les ressources
        try (Connection connection = DriverManager.getConnection(URL, LOGIN, PASS);
             Statement statement = connection.createStatement()) {

            // Exécution de la requête pour vider la table "recherche"
            statement.executeUpdate(deleteRecherchesQuery);
            // Exécution de la requête pour vider la table "connexion"
            statement.executeUpdate(deleteConnexionsQuery);

        } catch (SQLException e) {
            // Affichage de la pile d'exécution en cas d'erreur SQL
            e.printStackTrace();
        }
    }
}
