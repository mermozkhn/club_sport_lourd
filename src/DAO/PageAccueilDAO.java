package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;

// La classe PageAccueilDAO fournit des méthodes pour interagir avec la base de données liées à la page d'accueil de l'application
public class PageAccueilDAO {
    
    private static final String USER = "root"; // Nom d'utilisateur de la base de données
    private static final String PASSWORD = "root"; // Mot de passe de la base de données
    final static String URL   = "jdbc:mysql://localhost:3306/club_sport"; // URL de connexion à la base de données
    static Connection conn = null; // Connexion à la base de données
    
    // Initialisation du pilote JDBC lors du chargement de la classe
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    // Méthode pour établir la connexion à la base de données
    public static void dbConnect() {
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Connexion à la base de données établie avec succès.");
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Erreur lors de la connexion à la base de données : " + e.getMessage());
        }
    }
    
    // Méthode pour fermer la connexion à la base de données
    public static void dbClose() {
        if (conn != null) {
            try {
                conn.close();
                System.out.println("Connexion à la base de données fermée avec succès.");
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Erreur lors de la fermeture de la connexion à la base de données : " + e.getMessage());
            }
        }
    }
    
    // Méthode pour peupler les menus déroulants avec les données des clubs
    public void populateComboBoxes(JComboBox<String> communeComboBox, JComboBox<String> regionComboBox,
            JComboBox<String> departementComboBox, JComboBox<String> federationComboBox) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            
            // Récupération des données distinctes des communes
            String communeQuery = "SELECT DISTINCT nomcommune FROM clubs";
            stmt = conn.prepareStatement(communeQuery);
            rs = stmt.executeQuery();
            
            // Vider les menus déroulants avant de les peupler à nouveau
            communeComboBox.removeAllItems();
            regionComboBox.removeAllItems();
            departementComboBox.removeAllItems();
            federationComboBox.removeAllItems();
            
            // Ajout d'une option par défaut à chaque menu déroulant
            communeComboBox.addItem("Sélectionnez une commune");
            regionComboBox.addItem("Sélectionnez une région");
            departementComboBox.addItem("Sélectionnez un département");
            federationComboBox.addItem("Sélectionnez une fédération");
            
            // Parcours des résultats et ajout des communes dans le menu déroulant
            while (rs.next()) {
                communeComboBox.addItem(rs.getString("nomcommune"));
            }
            
            // Sélection de la première option par défaut
            communeComboBox.setSelectedIndex(0);
            
            // Récupération des données distinctes des régions
            String regionQuery = "SELECT DISTINCT region FROM clubs";
            stmt = conn.prepareStatement(regionQuery);
            rs = stmt.executeQuery();
            
            // Parcours des résultats et ajout des régions dans le menu déroulant
            while (rs.next()) {
                regionComboBox.addItem(rs.getString("region"));
            }
            
            // Sélection de la première option par défaut
            regionComboBox.setSelectedIndex(0);
            
            // Récupération des données distinctes des départements
            String departementQuery = "SELECT DISTINCT departement FROM clubs";
            stmt = conn.prepareStatement(departementQuery);
            rs = stmt.executeQuery();
            
            // Parcours des résultats et ajout des départements dans le menu déroulant
            while (rs.next()) {
                departementComboBox.addItem(rs.getString("departement"));
            }
            
            // Sélection de la première option par défaut
            departementComboBox.setSelectedIndex(0);
            
            // Récupération des données distinctes des fédérations
            String federationQuery = "SELECT DISTINCT federation FROM clubs";
            stmt = conn.prepareStatement(federationQuery);
            rs = stmt.executeQuery();
            
            // Parcours des résultats et ajout des fédérations dans le menu déroulant
            while (rs.next()) {
                federationComboBox.addItem(rs.getString("federation"));
            }
            
            // Sélection de la première option par défaut
            federationComboBox.setSelectedIndex(0);
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(rs, stmt, conn); // Fermeture des ressources
        }
    }
    
    //chercher des valeurs distinctes depuis bdd
    public List<String> getDistinctValues(String column) {
        List<String> values = new ArrayList<>();
        String query = "SELECT DISTINCT " + column + " FROM clubs";

        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement stmt = conn.prepareStatement(query);
             ResultSet rs = stmt.executeQuery()) {

            while (rs.next()) {
                values.add(rs.getString(column));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return values;
    }

    
    // Méthode pour obtenir le nombre de clubs correspondant à un terme de recherche donné
    public static int getClubCount(String searchTerm) {
        int clubCount = 0;
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement stmt = conn.prepareStatement("SELECT COUNT(*) FROM clubs WHERE nomclub LIKE ?");
        ) {
            stmt.setString(1, "%" + searchTerm + "%"); // Définition du paramètre de requête avec le terme de recherche fourni
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                clubCount = rs.getInt(1); // Récupération du nombre de clubs correspondant au terme de recherche
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return clubCount; // Retourne le nombre de clubs
    }

    // Méthode pour fermer les ressources (ResultSet, PreparedStatement, Connection)
    private void closeResources(ResultSet rs, PreparedStatement stmt, Connection conn) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
