package DAO;

import java.sql.*;
import java.util.ArrayList;
import Model.Club;
import Model.Evenement;

public class GestionClubDAO extends ConnectionDAO {

    public GestionClubDAO() {
        super();
    }

    /**
     * Permet d'ajouter un club dans la table Club.
     * 
     * @param club le club à ajouter
     * @return le nombre de lignes ajoutées dans la table
     */
    public int add(Club club) {
        Connection con = null;
        PreparedStatement ps = null;
        int returnValue = 0;

        try {
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            ps = con.prepareStatement("INSERT INTO Club(id_club, Code_commune, Code_federation, Nom_federation, departement, region, Total_club) VALUES (?, ?, ?, ?, ?, ?, ?)");
            ps.setInt(1, club.getId_club());
            ps.setString(2, club.getCode_commune());
            ps.setString(3, club.getCode_federation());
            ps.setString(4, club.getNom_federation());
            ps.setInt(5, club.getDepartement());
            ps.setString(6, club.getRegion());
            ps.setInt(7, club.getTotal_club());

            returnValue = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection(con, ps);
        }
        return returnValue;
    }

    /**
     * Permet de modifier un club dans la table Club.
     * 
     * @param club le club à modifier
     * @return le nombre de lignes modifiées dans la table
     */
    public int update(Club club) {
        Connection con = null;
        PreparedStatement ps = null;
        int returnValue = 0;

        try {
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            ps = con.prepareStatement("UPDATE Club SET Code_commune = ?, Code_federation = ?, Nom_federation = ?, departement = ?, region = ?, Total_club = ? WHERE id_club = ?");
            ps.setString(1, club.getCode_commune());
            ps.setString(2, club.getCode_federation());
            ps.setString(3, club.getNom_federation());
            ps.setInt(4, club.getDepartement());
            ps.setString(5, club.getRegion());
            ps.setInt(6, club.getTotal_club());
            ps.setInt(7, club.getId_club());

            returnValue = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection(con, ps);
        }
        return returnValue;
    }

    /**
     * Permet de supprimer un club par son ID dans la table Club.
     * 
     * @param id_club l'ID du club à supprimer
     * @return le nombre de lignes supprimées dans la table
     */
    public int delete(int id_club) {
        Connection con = null;
        PreparedStatement ps = null;
        int returnValue = 0;

        try {
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            ps = con.prepareStatement("DELETE FROM Club WHERE id_club = ?");
            ps.setInt(1, id_club);

            returnValue = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection(con, ps);
        }
        return returnValue;
    }

    /**
     * Permet de récupérer un club par son ID dans la table Club.
     * 
     * @param id_club l'ID du club à récupérer
     * @return le club trouvé, null si aucun club ne correspond à cet ID
     */
    public Club get(int id_club) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Club returnValue = null;

        try {
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            ps = con.prepareStatement("SELECT * FROM Club WHERE id_club = ?");
            ps.setInt(1, id_club);

            rs = ps.executeQuery();
            if (rs.next()) {
                returnValue = new Club(rs.getInt("id_club"),
                                       rs.getString("Code_commune"),
                                       rs.getString("Code_federation"),
                                       rs.getString("Nom_federation"),
                                       rs.getInt("departement"),
                                       rs.getString("region"),
                                       rs.getInt("Total_club"));
                
                // Récupérer les événements associés à ce club
                ArrayList<Evenement> evenements = getEvenementsForClub(id_club);
                returnValue.setEvenements(evenements);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection(con, ps, rs);
        }
        return returnValue;
    }

    /**
     * Permet de récupérer tous les événements associés à un club par son ID.
     * 
     * @param id_club l'ID du club
     * @return une liste d'événements associés à ce club
     */
    private ArrayList<Evenement> getEvenementsForClub(int id_club) {
        ArrayList<Evenement> evenements = new ArrayList<>();
        // Ici vous pouvez écrire la logique pour récupérer les événements associés à un club dans la base de données
        // Puis ajouter ces événements à la liste evenements
        return evenements;
    }

    // Autres méthodes de DAO pour gérer les événements associés à un club, telles que addEvenement, deleteEvenement, etc.
    
    /**
     * Permet de fermer la connexion à la base de données.
     * 
     * @param con la connexion
     * @param ps le PreparedStatement
     * @param rs le ResultSet
     */
    private void closeConnection(Connection con, PreparedStatement ps, ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de fermer la connexion à la base de données.
     * 
     * @param con la connexion
     * @param ps le PreparedStatement
     */
    private void closeConnection(Connection con, PreparedStatement ps) {
        closeConnection(con, ps, null);
    }
}
