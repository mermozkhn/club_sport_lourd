
package DAO;
import Model.Utilisateur;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;


import Model.Utilisateur;

public class UtilisateurDAO  extends ConnectionDAO{
	public UtilisateurDAO() {
		
	super();
}
	
	/**
	 * Permet d'ajouter un fournisseur dans la table Etudiant.
	 * Le mode est auto-commit par defaut : chaque insertion est validee
	 * 
	 * @param Utilisateurle fournisseur a ajouter
	 * @return retourne le nombre de lignes ajoutees dans la table
	 */
	public int add(Utilisateur utilisateur) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, chaque ? represente une valeur
			// a communiquer dans l'insertion.
			// les getters permettent de recuperer les valeurs des attributs souhaites
			ps = con.prepareStatement("INSERT INTO users(nom,prenom,email,motdepass,profil,fileName,file,domaine,fede,validate) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setString(1, utilisateur.getNom());
			ps.setString(2, utilisateur.getPrenom());
			ps.setString(3, utilisateur.getEmail());
			ps.setString(4, utilisateur.getMotdepass());
			ps.setString(5, utilisateur.getProfil());
			ps.setString(6, utilisateur.getFileName());
			ps.setBlob(7, utilisateur.getFile());
			ps.setString(8, utilisateur.getDomaine());
			ps.setString(9, utilisateur.getFede());
			ps.setString(10, utilisateur.getValidate());

			

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-00001"))
				System.out.println("Cet identifiant de fournisseur existe dÃ©jÃ . Ajout impossible !");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}
	

    public List<Utilisateur> recuperer() {
        List<Utilisateur> utilisateurs = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            String sql = "SELECT * FROM users";
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                if (!"admin".equals(rs.getString("profil"))) {
                    int id = rs.getInt("idusers");
                    String nom = rs.getString("nom");
                    String prenom = rs.getString("prenom");
                    String email = rs.getString("email");
                    String motDePass = rs.getString("motdepass");
                    String profil = rs.getString("profil");
                    String setFileName = rs.getString("fileName");
                    InputStream setFile = rs.getBinaryStream("file");
                    String Domaine = rs.getString("domaine");
                    String Valider = rs.getString("validate");

                    Utilisateur utilisateur = new Utilisateur(id, nom, prenom, email, motDePass, profil);
                    
                    utilisateur.setFileName(setFileName);
                    utilisateur.setFile(setFile);
                    utilisateur.setDomaine(Domaine);
                    utilisateur.setValidate(Valider);
                    utilisateurs.add(utilisateur);
                }
            }
            return utilisateurs;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (con != null) con.close();
            } catch (SQLException ignore) {
            }
        }

        return utilisateurs;
    }

	 
    public void telechargerFichier(String nom, String repertoireCible) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        Path cheminCible = Paths.get(repertoireCible);

        // S'assurer que le rÃ©pertoire existe
        try {
            if (!Files.exists(cheminCible)) {
                Files.createDirectories(cheminCible);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        try {
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            String sql = "SELECT fileName, file FROM users WHERE nom = ?";
            ps = con.prepareStatement(sql);
            ps.setString(1, nom);

            rs = ps.executeQuery();
            if (rs.next()) {
                String nomFichier = rs.getString("fileName");
                InputStream inputStream = rs.getBinaryStream("file");

                File fichierCible = new File(repertoireCible, nomFichier);
                try (FileOutputStream outputStream = new FileOutputStream(fichierCible)) {
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                    System.out.println("Le fichier a Ã©tÃ© tÃ©lÃ©chargÃ© avec succÃ¨s : " + fichierCible.getAbsolutePath());
                }
                inputStream.close();
            } else {
                System.out.println("Aucun fichier trouvÃ© avec le nom spÃ©cifiÃ©.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (con != null) con.close();
            } catch (Exception ignore) {
            }
        }
    }


	/**
	 * Permet de modifier un fournisseur dans la table Etudiant.
	 * Le mode est auto-commit par defaut : chaque modification est validee
	 * 
	 * @param Utilisateurle fournisseur a modifier
	 * @return retourne le nombre de lignes modifiees dans la table
	 */
	 public int update(int id, String valider) {
		    Connection con = null;
		    PreparedStatement ps = null;
		    int returnValue = 0;

		    try {
		        con = DriverManager.getConnection(URL, LOGIN, PASS);
		        // PrÃ©parez votre requÃªte SQL en mettant Ã  jour uniquement la colonne "valider"
		        ps = con.prepareStatement("UPDATE users SET validate = ? WHERE idusers = ?");
		        ps.setString(1, valider); // Remplacez le premier point d'interrogation par la valeur de "valider"
		        ps.setInt(2, id); // Remplacez le deuxiÃ¨me point d'interrogation par la valeur de "id"

		        // ExÃ©cutez la requÃªte
		        returnValue = ps.executeUpdate();
		    } catch (SQLException e) {
		        e.printStackTrace();
		    } finally {
		        // Fermez les ressources
		        try {
		            if (ps != null) {
		                ps.close();
		            }
		            if (con != null) {
		                con.close();
		            }
		        } catch (SQLException ignore) {
		        }
		    }
		    return returnValue;
		}


	/**
	 * Permet de supprimer un fournisseur par id dans la table Etudiant.
	 * Si ce dernier possede des articles, la suppression n'a pas lieu.
	 * Le mode est auto-commit par defaut : chaque suppression est validee
	 * 
	 * @param id l'id du UtilisateurÃ  supprimer
	 * @return retourne le nombre de lignes supprimees dans la table
	 */
	public int delete(int Id_user) {
		Connection con = null;
		PreparedStatement ps = null;
		int returnValue = 0;

		// connexion a la base de donnees
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			// preparation de l'instruction SQL, le ? represente la valeur de l'ID
			// a communiquer dans la suppression.
			// le getter permet de recuperer la valeur de l'ID du fournisseur
			ps = con.prepareStatement("DELETE FROM users WHERE IDUSERS= ?");
			ps.setInt(1, Id_user);

			// Execution de la requete
			returnValue = ps.executeUpdate();

		} catch (Exception e) {
			if (e.getMessage().contains("ORA-02292"))
				System.out.println("Ce fournisseur possede des articles, suppression impossible !"
						         + " Supprimer d'abord ses articles ou utiiser la mÃ©thode de suppression avec articles.");
			else
				e.printStackTrace();
		} finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}


	/**
	 * Permet de recuperer un fournisseur a partir de sa reference
	 * 
	 * @param reference la reference du fournisseur a recuperer
	 * @return le fournisseur trouve;
	 * 			null si aucun fournisseur ne correspond a cette reference
	 */
	public Utilisateur get(int Id_user) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Utilisateur returnValue = null;

		// connexion a la base de donnees
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM UtilisateurWHERE id = ?");
			ps.setInt(1, Id_user);

			// on execute la requete
			// rs contient un pointeur situe juste avant la premiere ligne retournee
			rs = ps.executeQuery();
			// passe a la premiere (et unique) ligne retournee
			if (rs.next()) {
				returnValue = new Utilisateur(rs.getInt("idusers"),
									       rs.getString("nom"),
									       rs.getString("prenom"),
									       rs.getString("email"),
									       rs.getString("motdepass"),
									       rs.getString("profil"));
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (ps != null) {
					ps.close();
				}
			} catch (Exception ignore) {
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (Exception ignore) {
			}
		}
		return returnValue;
	}

	public Utilisateur userConnection(String E_mail, String enteredPassword) {
	    Connection con = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    Utilisateur returnValue = null;

	    // connexion Ã  la base de donnÃ©es
	    try {
	        con = DriverManager.getConnection(URL, LOGIN, PASS);
	        ps = con.prepareStatement("SELECT * FROM users WHERE email = ?");
	        ps.setString(1, E_mail);

	        // ExÃ©cution de la requÃªte
	        rs = ps.executeQuery();

	        // Si un utilisateur correspondant Ã  l'e-mail est trouvÃ©
	        if (rs.next()) {
	            // RÃ©cupÃ©ration du mot de passe hachÃ© stockÃ© dans la base de donnÃ©es
	            String hashedPasswordFromDB = rs.getString("motdepass");

	            // VÃ©rification si le mot de passe entrÃ© correspond au mot de passe hashÃ© dans la base de donnÃ©es
	            System.out.println("Mot de passe entrÃ© : " + enteredPassword);
	            System.out.println("Mot de passe hachÃ© depuis la base de donnÃ©es : " + hashedPasswordFromDB);
	            if (checkPassword(enteredPassword, hashedPasswordFromDB)) {
	                // Authentification rÃ©ussie, crÃ©ation de l'objet Utilisateur
	                System.out.println("Mot de passe correct !");
	                returnValue = new Utilisateur(rs.getInt("idusers"), rs.getString("nom"), rs.getString("prenom"), rs.getString("email"), hashedPasswordFromDB, rs.getString("profil"));
	            } else {
	                System.out.println("Mot de passe incorrect !");
	            }
	        }
	    } catch (SQLException ee) {
	        ee.printStackTrace();
	    } finally {
	        // fermeture du ResultSet, du PreparedStatement et de la Connexion
	        try {
	            if (rs != null) {
	                rs.close();
	            }
	        } catch (SQLException ignore) {
	        }
	        try {
	            if (ps != null) {
	                ps.close();
	            }
	        } catch (SQLException ignore) {
	        }
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (SQLException ignore) {
	        }
	    }
	    return returnValue;
	}
	private boolean checkPassword(String enteredPassword, String hashedPasswordFromDB) {
	    return BCrypt.checkpw(enteredPassword, hashedPasswordFromDB);
	}

	public void insertUtilisateur(int id, String nom, String prenom, String email, String mdp, String profil) {
        String query = "INSERT INTO utilisateur (id, nom, prenom, email, mdp, profil) VALUES (?, ?, ?, ?, ?, ?)";
        try (Connection conn = DriverManager.getConnection(URL, LOGIN, PASS);
             PreparedStatement pstmt = conn.prepareStatement(query)) {
            pstmt.setInt(1, id);
            pstmt.setString(2, nom);
            pstmt.setString(3, prenom);
            pstmt.setString(4, email);
            pstmt.setString(5, mdp);
            pstmt.setString(6, profil);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
	public String getProfil(String E_mail, String enteredPassword) {
	    Connection con = null;
	    PreparedStatement ps = null;
	    ResultSet rs = null;
	    String returnValue = null;

	    // connexion Ã  la base de donnÃ©es
	    try {
	        con = DriverManager.getConnection(URL, LOGIN, PASS);
	        ps = con.prepareStatement("SELECT motdepass, profil FROM users WHERE email = ?");

	        ps.setString(1, E_mail);

	        // exÃ©cution de la requÃªte
	        rs = ps.executeQuery();
	        // si l'utilisateur est trouvÃ© dans la base de donnÃ©es
	        if (rs.next()) {
	            String hashedPasswordFromDB = rs.getString("motdepass");
	            // vÃ©rification du mot de passe hashÃ©
	            if (BCrypt.checkpw(enteredPassword, hashedPasswordFromDB)) {
	                // si les mots de passe correspondent, rÃ©cupÃ©rer le profil de l'utilisateur
	                returnValue = rs.getString("profil");
	            }
	        }
	    } catch (Exception ee) {
	        ee.printStackTrace();
	    } finally {
	        // fermeture du ResultSet, du PreparedStatement et de la Connexion
	        try {
	            if (rs != null) {
	                rs.close();
	            }
	        } catch (Exception ignore) {
	        }
	        try {
	            if (ps != null) {
	                ps.close();
	            }
	        } catch (Exception ignore) {
	        }
	        try {
	            if (con != null) {
	                con.close();
	            }
	        } catch (Exception ignore) {
	        }
	    }
	    return returnValue;
	}
/**
	/**
	 * Permet de recuperer tous les fournisseurs stockes dans la table fournisseur
	 * 
	 * @return une ArrayList de fournisseur
	 */
	


	/**
	 * ATTENTION : Cette mÃ©thode n'a pas vocation Ã  Ãªtre executÃ©e lors d'une utilisation normale du programme !
	 * Elle existe uniquement pour TESTER les mÃ©thodes Ã©crites au-dessus !
	 * 
	 * @param args non utilisÃ©s
	 * @throws SQLException si une erreur se produit lors de la communication avec la BDD
	 */

	public boolean addUser(String nom, String prenom, String email, String motdepass, String profil, String fileName, InputStream fileContent, String domaine, String value) {
        PreparedStatement stmt = null;
        try {
            dbConnect();
            if (!connected) {
                System.err.println("La connexion Ã  la base de donnÃ©es n'a pas pu Ãªtre Ã©tablie.");
                return false;
            }

            String query = "INSERT INTO users (nom, prenom, email, motdepass, profil, fileName, file, domaine, fede, validate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, nom);
            stmt.setString(2, prenom);
            stmt.setString(3, email);
            stmt.setString(4, motdepass);
            stmt.setString(5, profil);
            stmt.setString(6, fileName);
            if (fileContent != null) {
                stmt.setBlob(7, fileContent);
            }
            stmt.setString(8, domaine);
            stmt.setString(9, value);
            stmt.setString(10, "false");
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            System.err.println("Erreur lors de l'ajout de l'utilisateur : " + e.getMessage());
            e.printStackTrace();
            return false;
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    System.err.println("Erreur lors de la fermeture de la session : " + e.getMessage());
                }
            }
            dbClose();
        }
    }

    // Modifie cette mÃ©thode pour retourner un String au lieu de boolean
    public String authenticateUser(String email, String motdepass, String profil) {
        PreparedStatement stmt = null;
        String pass="";
        ResultSet rs = null;
        try {
            dbConnect();
            String query = "SELECT * FROM users WHERE email = ? AND profil = ? AND validate = ?";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, email);
            stmt.setString(2, profil);
            stmt.setString(3, "true");
            rs = stmt.executeQuery();
            if (rs.next()) {
            	pass=rs.getString("motdepass");
            	Boolean isPasswordCorrect = checkPassword(motdepass,pass);
            	if(isPasswordCorrect) {
            		return rs.getString("nom"); // Retourne le nom de l'utilisateur
            	}
            }
            return null; // Retourne null si l'utilisateur n'est pas trouvÃ©
        } catch (SQLException e) {
            System.err.println("Erreur d'authentification : " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (rs != null) rs.close();
               if (stmt != null) stmt.close();
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture : " + e.getMessage());
            }
        }
    }
	   
	}


