package DAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LicencieDAO {
    // Constantes pour les informations de connexion à la base de données
    private static final String DB_URL = "jdbc:mysql://localhost:3306/club_sport";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "root";

    // Méthode pour obtenir le nombre total de licenciés masculins par fédération et commune
    public int getTotalMaleLicenciesByLocation(String federation, String commune) {
        int totalMaleLicencies = 0; // Variable pour stocker le résultat
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Établir la connexion à la base de données
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // Requête SQL pour calculer le total des licenciés masculins par tranches d'âge
            String sql = "SELECT SUM(H_1_4_ans + H_5_9_ans + H_10_14_ans + H_15_19_ans + H_20_24_ans + H_25_29_ans + H_30_34_ans + H_35_39_ans + H_40_44_ans + H_45_49_ans + H_50_54_ans + H_55_59_ans + H_60_64_ans + H_65_69_ans + H_70_74_ans + H_75_79_ans + H_80_99_ans) AS totalMaleLicencies FROM licence WHERE Federation = ? AND Commune = ?";
            stmt = conn.prepareStatement(sql);
            // Définir les paramètres de la requête
            stmt.setString(1, federation);
            stmt.setString(2, commune);
            // Exécuter la requête
            rs = stmt.executeQuery();
            if (rs.next()) {
                // Récupérer le résultat
                totalMaleLicencies = rs.getInt("totalMaleLicencies");
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer les exceptions SQL
        } finally {
            // Fermer les ressources dans le bloc finally pour s'assurer qu'elles sont toujours fermées
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return totalMaleLicencies;
    }

    // Méthode pour obtenir le nombre total de licenciées féminines par fédération et commune
    public int getTotalFemaleLicenciesByLocation(String federation, String commune) {
        int totalFemaleLicencies = 0; // Variable pour stocker le résultat
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Établir la connexion à la base de données
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // Requête SQL pour calculer le total des licenciées féminines par tranches d'âge
            String sql = "SELECT SUM(F_1_4_ans + F_5_9_ans + F_10_14_ans + F_15_19_ans + F_20_24_ans + F_25_29_ans + F_30_34_ans + F_35_39_ans + F_40_44_ans + F_45_49_ans + F_50_54_ans + F_55_59_ans + F_60_64_ans + F_65_69_ans + F_70_74_ans + F_75_79_ans + F_80_99_ans) AS totalFemaleLicencies FROM licence WHERE Federation = ? AND Commune = ?";
            stmt = conn.prepareStatement(sql);
            // Définir les paramètres de la requête
            stmt.setString(1, federation);
            stmt.setString(2, commune);
            // Exécuter la requête
            rs = stmt.executeQuery();
            if (rs.next()) {
                // Récupérer le résultat
                totalFemaleLicencies = rs.getInt("totalFemaleLicencies");
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer les exceptions SQL
        } finally {
            // Fermer les ressources dans le bloc finally pour s'assurer qu'elles sont toujours fermées
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return totalFemaleLicencies;
    }

    // Méthode pour obtenir le nombre total de licenciés masculins par région et fédération
    public int getTotalMaleLicenciesByRegionAndFederation(String region, String federation) {
        int totalMaleLicencies = 0; // Variable pour stocker le résultat
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Établir la connexion à la base de données
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // Requête SQL pour calculer le total des licenciés masculins par tranches d'âge
            String sql = "SELECT SUM(H_1_4_ans + H_5_9_ans + H_10_14_ans + H_15_19_ans + H_20_24_ans + H_25_29_ans + H_30_34_ans + H_35_39_ans + H_40_44_ans + H_45_49_ans + H_50_54_ans + H_55_59_ans + H_60_64_ans + H_65_69_ans + H_70_74_ans + H_75_79_ans + H_80_99_ans) AS totalMaleLicencies FROM licence WHERE Region = ? AND Federation = ?";
            stmt = conn.prepareStatement(sql);
            // Définir les paramètres de la requête
            stmt.setString(1, region);
            stmt.setString(2, federation);
            // Exécuter la requête
            rs = stmt.executeQuery();
            if (rs.next()) {
                // Récupérer le résultat
                totalMaleLicencies = rs.getInt("totalMaleLicencies");
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer les exceptions SQL
        } finally {
            // Fermer les ressources dans le bloc finally pour s'assurer qu'elles sont toujours fermées
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return totalMaleLicencies;
    }

    // Méthode pour obtenir le nombre total de licenciées féminines par région et fédération
    public int getTotalFemaleLicenciesByRegionAndFederation(String region, String federation) {
        int totalFemaleLicencies = 0; // Variable pour stocker le résultat
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Établir la connexion à la base de données
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // Requête SQL pour calculer le total des licenciées féminines par tranches d'âge
            String sql = "SELECT SUM(F_1_4_ans + F_5_9_ans + F_10_14_ans + F_15_19_ans + F_20_24_ans + F_25_29_ans + F_30_34_ans + F_35_39_ans + F_40_44_ans + F_45_49_ans + F_50_54_ans + F_55_59_ans + F_60_64_ans + F_65_69_ans + F_70_74_ans + F_75_79_ans + F_80_99_ans) AS totalFemaleLicencies FROM licence WHERE Region = ? AND Federation = ?";
            stmt = conn.prepareStatement(sql);
            // Définir les paramètres de la requête
            stmt.setString(1, region);
            stmt.setString(2, federation);
            // Exécuter la requête
            rs = stmt.executeQuery();
            if (rs.next()) {
                // Récupérer le résultat
                totalFemaleLicencies = rs.getInt("totalFemaleLicencies");
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer les exceptions SQL
        } finally {
            // Fermer les ressources dans le bloc finally pour s'assurer qu'elles sont toujours fermées
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return totalFemaleLicencies;
    }

    // Méthode pour obtenir la liste des communes
    public List<String> getCommunes() {
        List<String> communes = new ArrayList<>(); // Liste pour stocker les résultats
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Établir la connexion à la base de données
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // Requête SQL pour récupérer les communes distinctes
            String sql = "SELECT DISTINCT Commune FROM licence";
            stmt = conn.prepareStatement(sql);
            // Exécuter la requête
            rs = stmt.executeQuery();
            while (rs.next()) {
                // Ajouter chaque commune à la liste
                communes.add(rs.getString("Commune"));
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer les exceptions SQL
        } finally {
            // Fermer les ressources dans le bloc finally pour s'assurer qu'elles sont toujours fermées
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return communes;
    }

    // Méthode pour obtenir la liste des régions
    public List<String> getRegions() {
        List<String> regions = new ArrayList<>(); // Liste pour stocker les résultats
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Établir la connexion à la base de données
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // Requête SQL pour récupérer les régions distinctes
            String sql = "SELECT DISTINCT Region FROM licence";
            stmt = conn.prepareStatement(sql);
            // Exécuter la requête
            rs = stmt.executeQuery();
            while (rs.next()) {
                // Ajouter chaque région à la liste
                regions.add(rs.getString("Region"));
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer les exceptions SQL
        } finally {
            // Fermer les ressources dans le bloc finally pour s'assurer qu'elles sont toujours fermées
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return regions;
    }

    // Méthode pour obtenir la liste des départements
    public List<String> getDepartements() {
        List<String> departements = new ArrayList<>(); // Liste pour stocker les résultats
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Établir la connexion à la base de données
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // Requête SQL pour récupérer les départements distincts
            String sql = "SELECT DISTINCT Departement FROM licence";
            stmt = conn.prepareStatement(sql);
            // Exécuter la requête
            rs = stmt.executeQuery();
            while (rs.next()) {
                // Ajouter chaque département à la liste
                departements.add(rs.getString("Departement"));
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer les exceptions SQL
        } finally {
            // Fermer les ressources dans le bloc finally pour s'assurer qu'elles sont toujours fermées
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return departements;
    }

    // Méthode pour obtenir la liste des fédérations
    public List<String> getFederations() {
        List<String> federations = new ArrayList<>(); // Liste pour stocker les résultats
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // Établir la connexion à la base de données
            conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // Requête SQL pour récupérer les fédérations distinctes
            String sql = "SELECT DISTINCT Federation FROM licence";
            stmt = conn.prepareStatement(sql);
            // Exécuter la requête
            rs = stmt.executeQuery();
            while (rs.next()) {
                // Ajouter chaque fédération à la liste
                federations.add(rs.getString("Federation"));
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Gérer les exceptions SQL
        } finally {
            // Fermer les ressources dans le bloc finally pour s'assurer qu'elles sont toujours fermées
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return federations;
    }
}
