package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import Model.User;

public class GestionUserMdpDAO {
    
	private static final String USER = "root";
	private static final String PASSWORD = "root";
	final static String URL   = "jdbc:mysql://localhost:3306/club_sport";
	static Boolean connected = false;
	static Connection conn= null;
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			}
	}
	
	private static void dbConnect() {
		try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(URL, USER, PASSWORD); // Établissement de la connexion à la base de données
            System.out.println("Connexion à la base de données établie avec succès.");
            connected = true;
        } catch (ClassNotFoundException e) {
            System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
        } catch (SQLException e) {
            System.err.println("Erreur lors de la connexion à la base de données : " + e.getMessage());
        }
	}
	private static void dbClose() {
		 if (conn != null) {
	            try {
	            	connected = false;
	                conn.close(); // Fermeture de la connexion à la base de données
	                System.out.println("Connexion à la base de données fermée avec succès.");
	            } catch (SQLException e) {
	            	e.printStackTrace();
	                System.err.println("Erreur lors de la fermeture de la connexion à la base de données : " + e.getMessage());
	            }
	           
		 }
	}
        
     public static User sele(String email) {
  
    	 try {
    		 dbConnect();
    		 
             String query = "SELECT * FROM club_sport.users WHERE email = ?";
             PreparedStatement stmt;
             ResultSet rs;
             stmt = conn.prepareStatement(query);
             stmt.setString(1, email);
             rs = stmt.executeQuery();
             
             if (rs.next()) {
                 // Récupération des informations de l'utilisateur depuis la base de données
                 String firstName = rs.getString("prenom");
                 String lastName = rs.getString("nom");
                 String password = rs.getString("motdepass");
                 
                 // Création de l'objet User
                 User user = new User(firstName, lastName, password);
                 return user;
             }
         } catch (SQLException e) {
             e.printStackTrace();
         } finally {
             // Fermeture des ressource
                 dbClose();
         }
         
         return null;
     }
     
    // Méthode pour supprimer un utilisateur de la base de données
    public static void deleteUser(String email) {
        Connection conn = null;
        PreparedStatement stmt = null;
        
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            String query = "DELETE FROM users WHERE email = ?";
            stmt = conn.prepareStatement(query);
            stmt.setString(1, email);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // Fermeture des ressources
            try {
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
