package DAO;

 
import java.sql.*;
 
import java.util.ArrayList;
 
import java.util.List;
public class ClubDAO {
 
    private Connection conn;
    // Constructeur
 
    public ClubDAO() {
 
        // Initialisation de la connexion à la base de données
 
        try {
 
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/club_sport", "root", "root");
 
        } catch (SQLException e) {
 
            e.printStackTrace();
 
        }
 
    }
    // Méthode pour récupérer la liste des communes uniques depuis la base de données
 
    public List<String> getUniqueCommunes() throws SQLException {
 
        List<String> communes = new ArrayList<>();
 
        String query = "SELECT DISTINCT nomcommune FROM clubs";
 
        try (Statement stmt = conn.createStatement();
 
             ResultSet rs = stmt.executeQuery(query)) {
 
            while (rs.next()) {
 
                communes.add(rs.getString("nomcommune"));
 
            }
 
        }
 
        return communes;
 
    }
    // Méthode pour récupérer la liste des fédérations uniques depuis la base de données
 
    public List<String> getUniqueFederations() throws SQLException {
 
        List<String> federations = new ArrayList<>();
 
        String query = "SELECT DISTINCT federation FROM clubs";
 
        try (Statement stmt = conn.createStatement();
 
             ResultSet rs = stmt.executeQuery(query)) {
 
            while (rs.next()) {
 
                federations.add(rs.getString("federation"));
 
            }
 
        }
 
        return federations;
 
    }
 
    // Méthode pour récupérer la liste des département uniques depuis la base de données
 
    public List<String> getUniqueDepartement() throws SQLException {
 
        List<String> departement = new ArrayList<>();
 
        String query = "SELECT DISTINCT departement FROM clubs";
 
        try (Statement stmt = conn.createStatement();
 
             ResultSet rs = stmt.executeQuery(query)) {
 
            while (rs.next()) {
 
                departement.add(rs.getString("departement"));
 
            }
 
        }
 
        return departement;
 
    }
 
    // Méthode pour récupérer la liste des regions uniques depuis la base de données
 
    public List<String> getUniqueRegion() throws SQLException {
 
        List<String> region = new ArrayList<>();
 
        String query = "SELECT DISTINCT region FROM clubs";
 
        try (Statement stmt = conn.createStatement();
 
             ResultSet rs = stmt.executeQuery(query)) {
 
            while (rs.next()) {
 
               region.add(rs.getString("region"));
 
            }
 
        }
 
        return region;
 
    }
 
    /*
 
// Méthode pour obtenir le nombre total de clubs en fonction des critères sélectionnés
 
    public int getTotalClubsByCriteria(String nomcommune, String federation, String region, String departement) throws SQLException {
 
        int totalclub = 0;
 
        // Si les critères commune et fédération sont fournis, utilisons le nombre de clubs par fédération dans une commune
 
        if (nomcommune != null && !nomcommune.isEmpty() && federation != null && !federation.isEmpty()) {
 
            totalclub= getClubsInFederationByCommune(nomcommune, federation);
 
        } else {
 
            StringBuilder queryBuilder = new StringBuilder("SELECT COUNT(*) AS total FROM clubs WHERE 1=1");
 
            if (nomcommune != null && !nomcommune.isEmpty()) {
 
                queryBuilder.append(" AND nomcommune = '").append(nomcommune).append("'");
 
            }
 
            if (federation != null && !federation.isEmpty()) {
 
                queryBuilder.append(" AND federation = '").append(federation).append("'");
 
            }
 
            if (region != null && !region.isEmpty()) {
 
                queryBuilder.append(" AND region = '").append(region).append("'");
 
            }
 
            if (departement != null && !departement.isEmpty()) {
 
                queryBuilder.append(" AND departement = '").append(departement).append("'");
 
            }
            String query = queryBuilder.toString();
 
            try (PreparedStatement pstmt = conn.prepareStatement(query);
 
                 ResultSet rs = pstmt.executeQuery()) {
 
                if (rs.next()) {
 
                    totalclub= rs.getInt("total");
 
                }
 
            }
 
        }
 
        return totalclub;
 
    }*/
    // Méthode pour obtenir le nombre de clubs dans une fédération pour une commune donnée
/*
    public String getClubsInFederationByCommune(String federation, String nomcommune,String  departement,String  region) throws SQLException {
 
        String clubsInFederation = null;
 
        String query = "SELECT total FROM club_sport.clubs WHERE federation = ? AND nomcommune= ? AND departement= ? AND region= ? ";
 
        try (PreparedStatement pstmt = conn.prepareStatement(query)) {
 
            pstmt.setString(1, federation);
 
            pstmt.setString(2, nomcommune);
            pstmt.setString(3,   departement);
 
            pstmt.setString(4, region);

 
            try (ResultSet rs = pstmt.executeQuery()) {
 
                if (rs.next()) {
 
                    clubsInFederation = rs.getString("total");
 
                }
 
            }
 
        } catch (SQLException e) {
 
            // Affichez ou journalisez l'erreur SQL
 
            System.err.println("Erreur SQL : " + e.getMessage());
 
            System.err.println("Code SQLState : " + e.getSQLState());
 
            throw e; // Re-lancez l'exception pour signaler l'erreur
 
        }
 
        return clubsInFederation;
 
    }
*/
 
 
    public String getClubsInFederationByCommune(String federation, String nomcommune, String region) throws SQLException {
        String clubsInFederation = null;
 
        String query = "SELECT SUM(total) AS total_clubs FROM club_sport.clubs WHERE federation = ?";
 
        // Ajoute les critères supplémentaires si présents
        if (nomcommune != null) {
            query += " AND nomcommune = ?";
        } else if (region != null) {
            query += " AND region = ?";
        }
 
        try (PreparedStatement pstmt = conn.prepareStatement(query)) {
            pstmt.setString(1, federation);
            if (nomcommune != null || region != null) {
                pstmt.setString(2, nomcommune != null ? nomcommune : region);
            }
 
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    clubsInFederation = rs.getString("total_clubs");
                }
            }
        } catch (SQLException e) {
            // Affichez ou journalisez l'erreur SQL
            System.err.println("Erreur SQL : " + e.getMessage());
            System.err.println("Code SQLState : " + e.getSQLState());
            throw e; // Re-lancez l'exception pour signaler l'erreur
        }
 
        return clubsInFederation;
    }
}