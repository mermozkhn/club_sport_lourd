package DAO;
import java.awt.event.KeyAdapter; 
import java.awt.event.KeyEvent; 
import java.util.ArrayList; 
import javax.swing.JTextField; 
 
public class AutoComplete { 
    private JTextField textField; 
    private ArrayList<String> suggestions; 
 
    public AutoComplete(JTextField textField, ArrayList<String> suggestions) { 
        this.textField = textField; 
        this.suggestions = suggestions; 
        textField.addKeyListener(new KeyAdapter() { 
            @Override 
            public void keyReleased(KeyEvent e) { 
                String text = textField.getText().toLowerCase(); 
                for (String suggestion : suggestions) { 
                    if (suggestion.toLowerCase().startsWith(text)) { 
                        textField.setText(suggestion); 
                        textField.setCaretPosition(suggestion.length()); 
                        break; 
                    } 
                } 
            } 
        }); 
    } 
} 