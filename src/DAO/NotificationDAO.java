package DAO;
import Model.Notification;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement; // Assurez-vous d'importer cette classe
import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;




public class NotificationDAO extends ConnectionDAO {

    public NotificationDAO() {
        super();
    }

    public void insertNotification(String titre, String contenu) {
        dbConnect(); // Établir la connexion

        String sql = "INSERT INTO notification (Titre, Contenue) VALUES (?, ?)";

        try (Connection conn = this.conn;
             PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            pstmt.setString(1, titre);
            pstmt.setString(2, contenu);
            pstmt.executeUpdate();

            // Récupérer la clé générée (IDnotif)
            ResultSet generatedKeys = pstmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                int id = generatedKeys.getInt(1);
                System.out.println("Notification with ID " + id + " successfully inserted into the database.");
            } else {
                System.out.println("Failed to retrieve generated ID for the inserted notification.");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public List<Notification> recuperernotif() {
        List<Notification> notifications = new ArrayList<>();
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            con = DriverManager.getConnection(URL, LOGIN, PASS);
            String sql = "SELECT * FROM notification";
            stmt = con.prepareStatement(sql);
            rs = stmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("IDnotif"); // Remplacez "IDnotif" par le nom de votre colonne d'ID
                String titre = rs.getString("Titre");
                String contenue = rs.getString("Contenue");
               
                Notification notification = new Notification(id, titre, contenue);
                notifications.add(notification);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (con != null) con.close();
            } catch (SQLException ignore) {
            }
        }

        return notifications;
    }


    public static void main(String[] args) {
        NotificationDAO notificationDAO = new NotificationDAO();

        // Assuming you have a text file named 'exemple.txt' in the current directory
        String filePath = "exemple.txt";
       
    }
}