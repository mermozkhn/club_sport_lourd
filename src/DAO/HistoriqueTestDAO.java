package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class HistoriqueTestDAO {
    private static final String USER = "root";
    private static final String PASSWORD = "root";
    final static String URL   = "jdbc:mysql://localhost:3306/club_sport";
    static Connection conn = null;
    
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void dbConnect() {
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Connexion à la base de données établie avec succès.");
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Erreur lors de la connexion à la base de données : " + e.getMessage());
        }
    }
    
    public static void dbClose() {
        if (conn != null) {
            try {
                conn.close();
                System.out.println("Connexion à la base de données fermée avec succès.");
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Erreur lors de la fermeture de la connexion à la base de données : " + e.getMessage());
            }
        }
    }
    
    public static void storeRecherche(String recherche) {
        dbConnect();
        try {
            String query = "INSERT INTO recherche (recherche) VALUES (?)";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, recherche);
            stmt.executeUpdate();
            System.out.println("Recherche stockée avec succès : " + recherche);
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Erreur lors du stockage de la recherche : " + e.getMessage());
        } finally {
            dbClose();
        }
    }
}
