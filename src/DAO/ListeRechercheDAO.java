package DAO;
 
 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
 
import javax.swing.JComboBox;
 
public class ListeRechercheDAO {
 
    private static final String USER = "root";
    private static final String PASSWORD = "root";
    final static String URL = "jdbc:mysql://localhost:3306/club_sport";
    static Connection conn = null;
 
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
 
    public static void dbConnect() {
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Connexion à la base de données établie avec succès.");
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Erreur lors de la connexion à la base de données : " + e.getMessage());
        }
    }
 
    public static void dbClose() {
        if (conn != null) {
            try {
                conn.close();
                System.out.println("Connexion à la base de données fermée avec succès.");
            } catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Erreur lors de la fermeture de la connexion à la base de données : " + e.getMessage());
            }
        }
    }
 
    public void populateComboBoxes(JComboBox<String> communeComboBox, JComboBox<String> regionComboBox,
            JComboBox<String> departementComboBox, JComboBox<String> federationComboBox) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        dbConnect();
        try {
            conn = DriverManager.getConnection(URL, USER, PASSWORD);
 
            // Récupération des données distinctes des communes
            String communeQuery = "SELECT DISTINCT nomcommune FROM clubs";
            stmt = conn.prepareStatement(communeQuery);
            rs = stmt.executeQuery();
 
            // Vider les menus déroulants avant de les peupler à nouveau
            communeComboBox.removeAllItems();
            regionComboBox.removeAllItems();
            departementComboBox.removeAllItems();
            federationComboBox.removeAllItems();
 
            // Ajout d'une option par défaut à chaque menu déroulant
            communeComboBox.addItem("Sélectionnez une commune");
            regionComboBox.addItem("Sélectionnez une région");
            departementComboBox.addItem("Sélectionnez un département");
            federationComboBox.addItem("Sélectionnez une fédération");
 
            // Parcours des résultats et ajout des communes dans le menu déroulant
            while (rs.next()) {
                communeComboBox.addItem(rs.getString("nomcommune"));
            }
 
            // Sélection de la première option par défaut
            communeComboBox.setSelectedIndex(0);
 
            // Ajout d'un événement pour le menu déroulant de la commune
            communeComboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String selectedCommune = (String) communeComboBox.getSelectedItem();
                    if (selectedCommune != null) {
                        updateFederationsByCommune(selectedCommune, federationComboBox);
                    }
                }
            });
 
            // Récupération des données distinctes des régions
            String regionQuery = "SELECT DISTINCT region FROM clubs";
            stmt = conn.prepareStatement(regionQuery);
            rs = stmt.executeQuery();
 
            // Parcours des résultats et ajout des régions dans le menu déroulant
            while (rs.next()) {
                regionComboBox.addItem(rs.getString("region"));
            }
 
            // Sélection de la première option par défaut
            regionComboBox.setSelectedIndex(0);
 
            // Ajout d'un événement pour le menu déroulant de la région
            regionComboBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String selectedRegion = (String) regionComboBox.getSelectedItem();
                    if (selectedRegion != null) {
                        updateFederationsByRegion(selectedRegion, federationComboBox);
                    }
                }
            });
 
            // Récupération des données distinctes des départements
            String departementQuery = "SELECT DISTINCT departement FROM clubs";
            stmt = conn.prepareStatement(departementQuery);
            rs = stmt.executeQuery();
 
            // Parcours des résultats et ajout des départements dans le menu déroulant
            while (rs.next()) {
                departementComboBox.addItem(rs.getString("departement"));
            }
 
            // Sélection de la première option par défaut
            departementComboBox.setSelectedIndex(0);
 
            // Récupération des données distinctes des fédérations
            String federationQuery = "SELECT DISTINCT federation FROM clubs";
            stmt = conn.prepareStatement(federationQuery);
            rs = stmt.executeQuery();
 
            // Parcours des résultats et ajout des fédérations dans le menu déroulant
            while (rs.next()) {
                federationComboBox.addItem(rs.getString("federation"));
            }
 
            // Sélection de la première option par défaut
            federationComboBox.setSelectedIndex(0);
 
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(rs, stmt, conn);
        }
    }
 
    // Méthode pour mettre à jour les fédérations en fonction de la commune sélectionnée
    private void updateFederationsByCommune(String selectedCommune, JComboBox<String> federationComboBox) {
        try {
            List<String> federationsInCommune = getFederationsInCommune(selectedCommune);
            // Mettre à jour le menu déroulant des fédérations avec les fédérations filtrées
            federationComboBox.removeAllItems();
            federationComboBox.addItem("Sélectionnez une fédération");
            for (String federation : federationsInCommune) {
                federationComboBox.addItem(federation);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
 
    // Méthode pour mettre à jour les fédérations en fonction de la région sélectionnée
    private void updateFederationsByRegion(String selectedRegion, JComboBox<String> federationComboBox) {
        try {
            List<String> federationsInRegion = getFederationsInRegion(selectedRegion);
            // Mettre à jour le menu déroulant des fédérations avec les fédérations filtrées
            federationComboBox.removeAllItems();
            federationComboBox.addItem("Sélectionnez une fédération");
            for (String federation : federationsInRegion) {
                federationComboBox.addItem(federation);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
 
    // Méthode pour récupérer les fédérations en fonction de la commune sélectionnée
    private List<String> getFederationsInCommune(String nomcommune) throws SQLException {
        List<String> federation = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement statement = conn.prepareStatement("SELECT DISTINCT federation FROM Clubs WHERE nomcommune = ?")) {
            statement.setString(1, nomcommune);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    federation.add(resultSet.getString("federation"));
                }
            }
        }
        return federation;
    }
 
    // Méthode pour récupérer les fédérations en fonction de la région sélectionnée
    private List<String> getFederationsInRegion(String region) throws SQLException {
        List<String> federations = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(URL, USER, PASSWORD);
             PreparedStatement statement = conn.prepareStatement("SELECT DISTINCT federation FROM Clubs WHERE region = ?")) {
            statement.setString(1, region);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    federations.add(resultSet.getString("federation"));
                }
            }
        }
        return federations;
    }
 
    // Méthode pour fermer les ressources (ResultSet, Statement, Connection)
    private void closeResources(ResultSet rs, PreparedStatement stmt, Connection conn) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}