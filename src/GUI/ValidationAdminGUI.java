package GUI;
import DAO.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import Model.Utilisateur;
import java.util.List;
import java.io.File;

public class ValidationAdminGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private JTable table;
    private DefaultTableModel model;
    private JButton btnValider;
    private JButton btnRefuser;
    private JButton btnTelecharger;
    private JButton btnRetour;
    private UtilisateurDAO utilisateurDAO;

    public ValidationAdminGUI() {
        utilisateurDAO = new UtilisateurDAO();
        initialize();
        afficherUtilisateurs();
    }

    private void initialize() {
        setTitle("Validation Admin");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        getContentPane().setLayout(new BorderLayout(0, 0));

        model = new DefaultTableModel(new String[]{"ID", "Nom", "Prénom", "Email", "Mot de passe", "Profil", "Titre", "Donnée", "Domaine", "Valider"}, 0);
        table = new JTable(model);

        JScrollPane scrollPane = new JScrollPane(table);
        getContentPane().add(scrollPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        btnValider = new JButton("Valider");
        btnRefuser = new JButton("Refuser");
        btnTelecharger = new JButton("Télécharger");
        btnRetour = new JButton("Retour");

        buttonPanel.add(btnValider);
        buttonPanel.add(btnRefuser);
        buttonPanel.add(btnTelecharger);
        buttonPanel.add(btnRetour);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        btnValider.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int row = table.getSelectedRow();
                if (row != -1) {
                    int id = (int) model.getValueAt(row, 0);
                    utilisateurDAO.update(id, "true");
                    model.setValueAt("true", row, 9);
                    table.repaint();
                } else {
                    JOptionPane.showMessageDialog(ValidationAdminGUI.this, "Veuillez sélectionner un utilisateur.");
                }
            }
        });

        btnRefuser.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int row = table.getSelectedRow();
                if (row != -1) {
                    int id = (int) model.getValueAt(row, 0);
                    utilisateurDAO.update(id, "false");
                    model.setValueAt("false", row, 9);
                    table.repaint();
                } else {
                    JOptionPane.showMessageDialog(ValidationAdminGUI.this, "Veuillez sélectionner un utilisateur.");
                }
            }
        });

        btnTelecharger.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String nom = JOptionPane.showInputDialog(ValidationAdminGUI.this, "Entrez le nom de l'utilisateur :");
                if (nom != null && !nom.isEmpty()) {
                    JFileChooser fileChooser = new JFileChooser();
                    fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    fileChooser.setDialogTitle("Choisissez le répertoire de téléchargement");

                    int userSelection = fileChooser.showSaveDialog(ValidationAdminGUI.this);
                    if (userSelection == JFileChooser.APPROVE_OPTION) {
                        File directory = fileChooser.getSelectedFile();
                        String repertoireCible = directory.getAbsolutePath() + File.separator + nom; // Combine le répertoire choisi avec le nom de fichier

                        utilisateurDAO.telechargerFichier(nom, repertoireCible);
                        JOptionPane.showMessageDialog(ValidationAdminGUI.this, "Téléchargement terminé !");
                    } else {
                        JOptionPane.showMessageDialog(ValidationAdminGUI.this, "Aucun répertoire choisi.");
                    }
                } else {
                    JOptionPane.showMessageDialog(ValidationAdminGUI.this, "Nom d'utilisateur invalide.");
                }
            }
        });

        btnRetour.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PageAccueilGUI pageAccueil = new PageAccueilGUI(); // Revenir à l'interface d'accueil
                pageAccueil.setVisible(true); // Afficher l'interface d'accueil
                dispose(); // Fermer l'interface actuelle
            }
        });
    }

    private void afficherUtilisateurs() {
        List<Utilisateur> utilisateurs = utilisateurDAO.recuperer();
        model.setRowCount(0);
        for (Utilisateur utilisateur : utilisateurs) {
            model.addRow(new Object[]{
                utilisateur.getIdusers(),
                utilisateur.getNom(),
                utilisateur.getPrenom(),
                utilisateur.getEmail(),
                utilisateur.getMotdepass(),
                utilisateur.getProfil(),
                utilisateur.getFileName(),
                utilisateur.getFile(),
                utilisateur.getDomaine(),
                utilisateur.getValidate() // Il est possible que vous souhaitiez afficher un chemin ou un autre attribut ici plutôt qu'un InputStream
            });
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new ValidationAdminGUI().setVisible(true);
            }
        });
    }
}
