package GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import DAO.UserSearchDAO;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

public class UserSearchGUI extends JFrame {
    private static final long serialVersionUID = 1L; // Numéro de série pour la compatibilité de la sérialisation
    private JButton btnAfficherConnexions; // Bouton pour afficher les connexions
    private JButton btnAfficherRecherches; // Bouton pour afficher les recherches
    private JButton btnRetour; // Bouton pour revenir à la page d'accueil
    private JButton btnSupprimerData; // Bouton pour supprimer les données
    private JTable table; // Tableau pour afficher les données
    private UserSearchDAO userSearchDAO; // DAO pour interagir avec la base de données

    public UserSearchGUI() {
        userSearchDAO = new UserSearchDAO(); // Initialisation du DAO

        setTitle("User Search GUI"); // Titre de la fenêtre
        setSize(800, 600); // Taille de la fenêtre
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Action de fermeture
        setLocationRelativeTo(null); // Centrer la fenêtre

        // Configuration de l'apparence (look and feel)
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            UIManager.put("control", new Color(64, 64, 64));
            UIManager.put("nimbusBase", new Color(45, 45, 45));
            UIManager.put("nimbusFocus", new Color(115, 164, 209));
            UIManager.put("nimbusLightBackground", new Color(64, 64, 64));
            UIManager.put("nimbusSelectionBackground", new Color(115, 164, 209));
            UIManager.put("text", new Color(200, 200, 200));
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Initialisation des boutons
        btnAfficherConnexions = new JButton("Afficher les connexions");
        btnAfficherRecherches = new JButton("Afficher les recherches");
        btnRetour = new JButton("Retour");
        btnSupprimerData = new JButton("Supprimer Data");

        // Ajout d'écouteurs d'événements aux boutons
        btnAfficherConnexions.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                afficherConnexions();
            }
        });

        btnAfficherRecherches.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                afficherRecherches();
            }
        });

        btnRetour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                retourAccueil();
            }
        });

        btnSupprimerData.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteData();
            }
        });

        // Création du panneau pour les boutons
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(btnAfficherConnexions);
        panel.add(btnAfficherRecherches);
        panel.add(btnRetour);
        panel.add(btnSupprimerData); // Ajout du bouton "Supprimer Data" au panneau

        // Initialisation du tableau
        table = new JTable();

        // Ajout du panneau et du tableau à la fenêtre
        add(panel, BorderLayout.NORTH);
        add(new JScrollPane(table), BorderLayout.CENTER);
    }

    // Méthode pour afficher les connexions
    private void afficherConnexions() {
        List<String[]> connexions = userSearchDAO.getConnexions(); // Récupération des connexions depuis le DAO
        afficherTable(connexions); // Affichage des données dans le tableau
    }

    // Méthode pour afficher les recherches
    private void afficherRecherches() {
        List<String[]> recherches = userSearchDAO.getRecherches(); // Récupération des recherches depuis le DAO
        afficherTable(recherches); // Affichage des données dans le tableau
    }

    // Méthode pour afficher les données dans le tableau
    private void afficherTable(List<String[]> data) {
        if (data.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Aucune donnée disponible.", "Information", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        // Récupération des noms des colonnes
        String[] columnNames = data.get(0);
        Vector<String> columnVector = new Vector<>(List.of(columnNames));
        Vector<Vector<String>> dataVector = new Vector<>();

        // Récupération des lignes de données
        for (int i = 1; i < data.size(); i++) {
            dataVector.add(new Vector<>(List.of(data.get(i))));
        }

        // Mise à jour du modèle de tableau avec les nouvelles données
        table.setModel(new DefaultTableModel(dataVector, columnVector));
    }

    // Méthode pour revenir à la page d'accueil
    private void retourAccueil() {
        dispose(); // Ferme la fenêtre actuelle
        PageAccueilGUI PageAccueilGUI = new PageAccueilGUI(); // Création de la fenêtre d'accueil
        PageAccueilGUI.setVisible(true); // Affichage de la fenêtre d'accueil
        dispose(); // Ferme la fenêtre actuelle (ligne redondante)
    }

    // Méthode pour supprimer les données des tables "recherche" et "connexion"
    private void deleteData() {
        int confirm = JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment supprimer toutes les données ?", "Confirmation", JOptionPane.YES_NO_OPTION);
        if (confirm == JOptionPane.YES_OPTION) {
            userSearchDAO.deleteData(); // Appel de la méthode pour supprimer les données
            JOptionPane.showMessageDialog(this, "Données supprimées avec succès.", "Information", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    // Méthode principale pour lancer l'application
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            UserSearchGUI frame = new UserSearchGUI(); // Création de la fenêtre principale
            frame.setVisible(true); // Affichage de la fenêtre principale
        });
    }
}
