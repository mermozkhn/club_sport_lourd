package GUI; 

 

import java.awt.Color; 

import java.awt.Cursor; 

import java.awt.Dimension; 

import java.awt.Font; 

import java.awt.GridBagConstraints; 

import java.awt.GridBagLayout; 

import java.awt.Insets; 

import java.awt.event.ActionEvent; 

import java.awt.event.ActionListener; 

import java.sql.SQLException; 

import javax.swing.JButton; 

import javax.swing.JComboBox; 

import javax.swing.JFrame; 

import javax.swing.JLabel; 

import javax.swing.JOptionPane; 

import javax.swing.JPanel; 

import javax.swing.SwingUtilities; 

import javax.swing.UIManager; 

 

import DAO.ClubDAO; 

import DAO.LicencieDAO; 

import DAO.ListeRechercheDAO; 

 

public class MaGUI extends JFrame { 

    private ClubDAO clubDAO; 

    private ListeRechercheDAO dao; 

    private LicencieDAO dao1; 

    private static final long serialVersionUID = 1L; 

    private final Font mainFont = new Font("Arial", Font.PLAIN, 18); 

 

    public MaGUI() { 

        try { 

            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); 

        } catch (Exception e) { 

            e.printStackTrace(); 

        } 

        clubDAO = new ClubDAO(); 

        dao = new ListeRechercheDAO(); 

        dao1 = new LicencieDAO(); 

        setTitle("Page d'Accueil"); 

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 

 

        // Création des boutons avec leurs textes correspondants 

        JButton btnRechercheClubs = createStyledButton("Recherche de clubs"); 

        JButton btnRechercheLicencies = createStyledButton("Recherche de licenciés"); 

 

        // Création des menus déroulants 

        JComboBox<String> communeComboBox = new JComboBox<>(); 

        JComboBox<String> regionComboBox = new JComboBox<>(); 

        JComboBox<String> departementComboBox = new JComboBox<>(); 

        JComboBox<String> federationComboBox = new JComboBox<>(); 

 

        // Récupération des données depuis la base de données pour les menus déroulants 

        dao.populateComboBoxes(communeComboBox, regionComboBox, departementComboBox, federationComboBox); 

 

        // Création du JPanel avec GridBagLayout comme gestionnaire de disposition 

        JPanel panel = new JPanel(new GridBagLayout()); 

        GridBagConstraints gbc = new GridBagConstraints(); 

        gbc.insets = new Insets(10, 10, 10, 10); // Ajout de la marge 

 

        // Ajustements pour les contraintes de grille 

        gbc.anchor = GridBagConstraints.WEST; // Alignement des libellés à gauche 

        gbc.fill = GridBagConstraints.HORIZONTAL; // Étirement horizontal des composants 

        gbc.weightx = 1.0; // Les composants occupent tout l'espace disponible horizontalement 

 

        // Ajout des éléments au panneau avec des contraintes de grille spécifiées 

        gbc.gridx = 0; 

        gbc.gridy = 0; 

        panel.add(btnRechercheClubs, gbc); 

 

        gbc.gridy++; 

        panel.add(btnRechercheLicencies, gbc); 

 

        gbc.gridy++; 

        panel.add(new JLabel("Recherche de clubs :"), gbc); 

 

        gbc.gridy++; 

        gbc.gridwidth = 1; 

        panel.add(communeComboBox, gbc); 

 

        gbc.gridx++; 

        panel.add(regionComboBox, gbc); 

 

        gbc.gridx = 0; 

        gbc.gridy++; 

        panel.add(departementComboBox, gbc); 

 

        gbc.gridx++; 

        panel.add(federationComboBox, gbc); 

 

        // Appliquer le style visuel avec la couleur de fond du panneau 

        panel.setBackground(Color.DARK_GRAY); 

 

        // Ajout du panneau à la fenêtre principale 

        add(panel); 

 

        // Ajustement de la taille de la fenêtre en fonction de son contenu et centrage sur l'écran 

        pack(); 

        setLocationRelativeTo(null); 

 

        // Rendre la fenêtre visible 

        setVisible(true); 

 

        // Ajouter un écouteur d'événements au bouton de recherche de clubs 

        btnRechercheClubs.addActionListener(new ActionListener() { 

            public void actionPerformed(ActionEvent e) { 

                // Récupérer les valeurs sélectionnées dans les JComboBox 

                

            	String commune = (String) communeComboBox.getSelectedItem(); 

                String region = (String) regionComboBox.getSelectedItem(); 

                String departement = (String) departementComboBox.getSelectedItem(); 

                String federation = (String) federationComboBox.getSelectedItem(); 

 

                if (clubDAO != null) { 

                    try { 

                        // Vérifier si une commune valide est sélectionnée 

                        if (commune != null && !commune.equals("Sélectionnez une commune") && federation != null && !federation.isEmpty()) { 

                            // Si commune et fédération sont sélectionnées 

                            String clubsInFederation = clubDAO.getClubsInFederationByCommune(federation, commune, null); 

                            JOptionPane.showMessageDialog(MaGUI.this, "Nombre total de clubs dans la commune " + commune + " pour la fédération " + federation + " : " + clubsInFederation); 

                        } else if (region != null && !region.isEmpty() && federation != null && !federation.isEmpty()) { 

                            // Si région et fédération sont sélectionnées 

                            String clubsInFederation = clubDAO.getClubsInFederationByCommune(federation, null, region); 

                            JOptionPane.showMessageDialog(MaGUI.this, "Nombre total de clubs dans la région " + region + " pour la fédération " + federation + " : " + clubsInFederation); 

                        } else { 

                            // Afficher un message d'erreur si les critères ne sont pas correctement sélectionnés 

                            JOptionPane.showMessageDialog(MaGUI.this, "Veuillez sélectionner une commune et une fédération, ou une région et une fédération."); 

                        } 

                    } catch (SQLException ex) { 

                        ex.printStackTrace(); 

                        JOptionPane.showMessageDialog(MaGUI.this, "Erreur: " + ex.getMessage()); 

                    } 

                } else { 

                    JOptionPane.showMessageDialog(MaGUI.this, "Erreur: clubDAO est null."); 

                } 

            } 

        }); 

         

        // Ajouter un écouteur d'événements au bouton de recherche de licenciés 

        btnRechercheLicencies.addActionListener(new ActionListener() { 

            public void actionPerformed(ActionEvent e) { 

                // Récupérer la commune sélectionnée dans le menu déroulant 

                String commu = (String) communeComboBox.getSelectedItem(); 

                // Récupérer la fédération sélectionnée dans le menu déroulant 

                String fede = (String) federationComboBox.getSelectedItem(); 

                //Récuperer la region selectionné dans le menu deroulant  

                String reg= (String) regionComboBox.getSelectedItem(); 

                

                

 

           /*  // Afficher les valeurs de fede et commu pour déboguer 

                System.out.println("Fédération : " + fede); 

                System.out.println("Commune : " + commu); 

                System.out.println("Region : " + reg); 

                */ 

                // Vérifier si commune et fédération sont sélectionnées 

             // Vérifier si commune et fédération sont sélectionnées 

                if ((fede != null && !fede.isEmpty()) && (commu != null && !commu.equals("Sélectionnez une commune"))) { 

                    // Obtenir le total des licenciés pour la fédération et la commune sélectionnées 

                    searchLicenciesByFederationAndCommune(fede, commu); 

                } 

 

                // Vérifier si region et fédération sont sélectionnées 

                if ((fede != null && !fede.isEmpty()) && (reg != null && !reg.isEmpty())) { 

                    // Obtenir le total des licenciés pour la région et la fédération sélectionnées 

                	if(reg.equals(("Sélectionnez une région"))) 

                	{ 

                		 

                	} 

                	else { 

                    searchLicenciesByRegionAndFederation(reg, fede); 

                } 

                } 

                // Afficher un message d'erreur si aucune région, commune ou fédération n'est sélectionnée 

                if (fede == null || fede.isEmpty()) { 

                    JOptionPane.showMessageDialog(MaGUI.this, "Veuillez sélectionner une fédération."); 

                } 

 

                if ((reg == null || reg.isEmpty()) && ("Sélectionnez une commune".equals(commu))) { 

                    commu = null; // Assurez-vous que commu est nulle si "Sélectionnez une commune" est choisi 

                } 

 

                if ((reg == null || reg.isEmpty()) && (commu == null || commu.isEmpty())) { 

                    JOptionPane.showMessageDialog(MaGUI.this, "Veuillez sélectionner une région ou une commune."); 

                } 

 

             // Afficher un message d'erreur si aucune région, commune ou fédération n'est sélectionnée 

                if (fede == null || fede.isEmpty()) { 

                    JOptionPane.showMessageDialog(MaGUI.this, "Veuillez sélectionner une fédération."); 

                } 

                if ((reg == null || reg.isEmpty()) && (commu == null || commu.isEmpty())) { 

                    JOptionPane.showMessageDialog(MaGUI.this, "Veuillez sélectionner une région ou une commune."); 

                } 

            } 

        }); 

    } 

 

    // Méthode pour exécuter la recherche de clubs et afficher le résultat 

    private void searchClubs(String federation, String commune, String departement, String region) { 

        try { 

            String clubsInFederation = clubDAO.getClubsInFederationByCommune(federation, commune, departement); 

            // Afficher le résultat dans une boîte de dialogue 

            JOptionPane.showMessageDialog(this, "Nombre total de clubs : " + clubsInFederation); 

        } catch (SQLException ex) { 

            ex.printStackTrace(); 

            JOptionPane.showMessageDialog(this, "Erreur: " + ex.getMessage()); 

        } 

    } 

/* 

    // Méthode pour exécuter la recherche de licenciés et afficher le résultat 

    private void searchLicencies(String federation, String commune) { 

        // Obtenir le total des licenciés hommes pour la fédération et la commune sélectionnées 

int totalMaleLicencies = dao1.getTotalMaleLicenciesByLocation(federation, commune); // Utilisation de l'instance dao 

int totalFemaleLicencies = dao1.getTotalFemaleLicenciesByLocation(federation, commune); // U 

// Afficher le total des licenciés hommes dans une boîte de dialogue 

JOptionPane.showMessageDialog(this, "Nombre total de licenciés hommes dans la fédération " + federation + " et la commune " + commune + " : " + totalMaleLicencies); 

JOptionPane.showMessageDialog(this, "Nombre total de licenciés femmes dans la fédération " + federation + " et la commune " + commune + " : " + totalFemaleLicencies); 

    } 

*/ 

  

 

    // Méthode pour rechercher les licenciés par région et fédération 

    private void searchLicenciesByRegionAndFederation(String region, String federation) { 

        int totalMaleLicencies = dao1.getTotalMaleLicenciesByRegionAndFederation(region, federation); 

        int totalFemaleLicencies = dao1.getTotalFemaleLicenciesByRegionAndFederation(region, federation); 

        // Afficher les résultats dans une boîte de dialogue 

        JOptionPane.showMessageDialog(this, "Nombre total de licenciés hommes dans la région " + region + " et la fédération " + federation + " : " + totalMaleLicencies); 

        JOptionPane.showMessageDialog(this, "Nombre total de licenciés femmes dans la région " + region + " et la fédération " + federation + " : " + totalFemaleLicencies); 

    } 

 

    // Méthode pour rechercher les licenciés par fédération et commune 

    private void searchLicenciesByFederationAndCommune(String federation, String commune) { 

        int totalMaleLicencies = dao1.getTotalMaleLicenciesByLocation(federation, commune); 

        int totalFemaleLicencies = dao1.getTotalFemaleLicenciesByLocation(federation, commune); 

        // Afficher les résultats dans une boîte de dialogue 

        JOptionPane.showMessageDialog(this, "Nombre total de licenciés hommes dans la fédération " + federation + " et la commune " + commune + " : " + totalMaleLicencies); 

        JOptionPane.showMessageDialog(this, "Nombre total de licenciés femmes dans la fédération " + federation + " et la commune " + commune + " : " + totalFemaleLicencies); 

    } 

 

     

    // Méthode pour créer un bouton avec un style personnalisé 

    private JButton createStyledButton(String text) { 

        JButton button = new JButton(text); 

        button.setFont(mainFont); 

        button.setBackground(Color.GRAY); 

        button.setForeground(Color.WHITE); 

        button.setFocusPainted(false); 

        button.setBorderPainted(false); 

        button.setOpaque(true); 

        button.setPreferredSize(new Dimension(300, 50)); 

        button.setCursor(new Cursor(Cursor.HAND_CURSOR)); 

        return button; 

    } 

 

public static void main(String[] args) { 

    // Assurez-vous que l'interface graphique est créée sur le thread de l'interface utilisateur 

    SwingUtilities.invokeLater(new Runnable() { 

        public void run() { 

            // Crée une instance de MaGUI 

            MaGUI maGUI = new MaGUI(); 

        } 

    }); 

} 

} 