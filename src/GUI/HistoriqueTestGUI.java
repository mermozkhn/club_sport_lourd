package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import DAO.HistoriqueTestDAO;

public class HistoriqueTestGUI extends JFrame {

    private static final long serialVersionUID = 1L;

    final private Font mainFont = new Font("Free Serif", Font.BOLD, 18);
    JTextField tfSearch;
    JButton btnSearch;

    public void initialize() {

        // Search Panel
        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new FlowLayout());
        searchPanel.setOpaque(false);

        JLabel lbSearch = new JLabel("Recherche:");
        lbSearch.setFont(mainFont);
        tfSearch = new JTextField(20);
        tfSearch.setFont(mainFont);
        btnSearch = new JButton("Rechercher");
        btnSearch.setFont(mainFont);

        searchPanel.add(lbSearch);
        searchPanel.add(tfSearch);
        searchPanel.add(btnSearch);

        // Main Panel
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setBackground(new Color(128, 128, 255));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        mainPanel.add(searchPanel, BorderLayout.CENTER);

        add(mainPanel);

        setTitle("Page de Recherche");
        setSize(500, 200);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
        setLocationRelativeTo(null);

        // Ajout d'un écouteur d'événements au bouton de recherche
        btnSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Récupération de la recherche saisie
                String recherche = tfSearch.getText();
                // Stockage de la recherche dans la base de données
                HistoriqueTestDAO.storeRecherche(recherche);
                // Réinitialisation du champ de recherche
                tfSearch.setText("");
            }
        });
    }

    public static void main(String[] args) {
        HistoriqueTestGUI HistoriqueTestGUI = new HistoriqueTestGUI();
        HistoriqueTestGUI.initialize();
    }
}
