package GUI;
import javax.swing.*;
import java.awt.*;

public class ProfilSportifGUI {
    private JFrame frame;
    private JLabel label;

    public ProfilSportifGUI() {
        initialize();
        frame.setVisible(true);
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(new BorderLayout());

        label = new JLabel("Page Profil Sportif");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(label, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                ProfilSportifGUI window = new ProfilSportifGUI();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
