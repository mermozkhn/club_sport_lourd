package GUI;

import DAO.NotificationDAO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class notificationGUI extends JFrame implements ActionListener {
    private JTextField titreField, contenuField;
    private JButton insertButton;
    private JLabel titreLabel, notificationLabel; // Ajout du JLabel pour le titre dans la page
    private NotificationDAO notificationDAO;

    public notificationGUI() {
        // Initialisation de la fenêtre
        setTitle("Envoyez une notification");
        setSize(300, 250);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        // Initialisation des composants
        titreLabel = new JLabel("Titre:"); // Ajout du label pour indiquer le champ "titre"
        titreField = new JTextField(20);
        contenuField = new JTextField(20);
        insertButton = new JButton("Insert");
        notificationLabel = new JLabel("");

        // Ajout des composants à la fenêtre
        JPanel panel = new JPanel(new GridLayout(0, 2)); // Utilisation d'une grille avec deux colonnes
        panel.add(new JLabel("Envoyez une notification")); // Titre de la fenêtre
        panel.add(new JLabel()); // Colonne vide pour l'alignement
        panel.add(titreLabel); // Ajout du libellé pour le champ "titre"
        panel.add(titreField);
        panel.add(new JLabel("Contenu:"));
        panel.add(contenuField);
        panel.add(insertButton);
        panel.add(new JLabel()); // Colonne vide pour l'alignement

        // Ajout du panneau à la fenêtre
        add(panel);

        // Ajout du label de notification à la fenêtre
        add(notificationLabel, BorderLayout.SOUTH);

        // Ajout de l'écouteur d'événements au bouton
        insertButton.addActionListener(this);

        // Initialisation du DAO
        notificationDAO = new NotificationDAO();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == insertButton) {
            String titre = titreField.getText();
            String contenu = contenuField.getText();

            // Insertion de la notification
            notificationDAO.insertNotification(titre, contenu);

            // Affichage de la notification insérée dans le JLabel
            notificationLabel.setText("Notification ajoutée : " + titre);

            // Effacer les champs après l'insertion
            titreField.setText("");
            contenuField.setText("");
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
        	notificationGUI frame = new notificationGUI(); // Création de la fenêtre principale
            frame.setVisible(true);
        });
    }
}