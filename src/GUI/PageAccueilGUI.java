package GUI;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import DAO.*;

public class PageAccueilGUI extends JFrame {
    private static final long serialVersionUID = 1L; // Pour la gestion de la sérialisation
    private final Font mainFont = new Font("Arial", Font.PLAIN, 18); // Police principale pour les boutons

    private ClubDAO clubDAO; // DAO pour les clubs
    private LicencieDAO dao1; // DAO pour les licenciés

    public PageAccueilGUI() {
        this.dao1 = new LicencieDAO();
        this.clubDAO = new ClubDAO();
        
        // Configuration du Look and Feel Nimbus avec des couleurs personnalisées
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            UIManager.put("control", new Color(64, 64, 64));
            UIManager.put("nimbusBase", new Color(45, 45, 45));
            UIManager.put("nimbusFocus", new Color(115, 164, 209));
            UIManager.put("nimbusLightBackground", new Color(64, 64, 64));
            UIManager.put("nimbusSelectionBackground", new Color(115, 164, 209));
            UIManager.put("text", new Color(200, 200, 200));
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setTitle("Page d'Accueil"); // Titre de la fenêtre
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Action par défaut à la fermeture

        // Création des boutons avec le style personnalisé
        JButton btnGestionComptes = createStyledButton("Gérer liste de comptes et MDP");
        JButton btnVisualiserHistorique = createStyledButton("Visualiser historique de connexion");
        JButton btnRechercheClubs = createStyledButton("Recherche de clubs");
        JButton btnRechercheLicencies = createStyledButton("Recherche de licenciés");
        JButton btnValiderPieceJointe = createStyledButton("Valider Pièce Jointe");
        JButton btnNotification = createStyledButton("Ajouter Notification");

        // Création des JComboBox pour la recherche
        JComboBox<String> communeComboBox = new JComboBox<>();
        JComboBox<String> regionComboBox = new JComboBox<>();
        JComboBox<String> departementComboBox = new JComboBox<>();
        JComboBox<String> federationComboBox = new JComboBox<>();

        // Rendre les JComboBox éditables
        communeComboBox.setEditable(true);
        regionComboBox.setEditable(true);
        departementComboBox.setEditable(true);
        federationComboBox.setEditable(true);

        // Configuration de l'auto-complétion pour les JComboBox
        setupAutoComplete(communeComboBox, dao1.getCommunes());
        setupAutoComplete(regionComboBox, dao1.getRegions());
        setupAutoComplete(departementComboBox, dao1.getDepartements());
        setupAutoComplete(federationComboBox, dao1.getFederations());

        // Création et configuration du panneau principal avec GridBagLayout
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 10, 10, 10);
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;

        // Ajout des composants au panneau principal
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        panel.add(btnValiderPieceJointe, gbc); // Ajout du bouton "Valider Pièce Jointe"

        gbc.gridy++;
        panel.add(btnGestionComptes, gbc);

        gbc.gridy++;
        panel.add(btnVisualiserHistorique, gbc);
        
        gbc.gridy++;
        panel.add(btnNotification, gbc);

        gbc.gridy++;
        panel.add(new JLabel("Recherche de clubs :"), gbc);

        gbc.gridy++;
        gbc.gridwidth = 1;
        panel.add(communeComboBox, gbc);

        gbc.gridx++;
        panel.add(regionComboBox, gbc);

        gbc.gridx = 0;
        gbc.gridy++;
        panel.add(departementComboBox, gbc);

        gbc.gridx++;
        panel.add(federationComboBox, gbc);

        gbc.gridx = 0;
        gbc.gridy++;
        gbc.gridwidth = 2;
        panel.add(btnRechercheClubs, gbc);

        gbc.gridx = 0;
        gbc.gridy++;
        gbc.gridwidth = 2;
        panel.add(btnRechercheLicencies, gbc);

        // Ajout des action listeners pour les boutons
        btnValiderPieceJointe.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ValidationAdminGUI validationAdminGUI = new ValidationAdminGUI(); // Créer l'interface de validation admin
                validationAdminGUI.setVisible(true); // Afficher l'interface de validation admin
                dispose(); // Fermer l'interface actuelle
            }
        });
        
        btnNotification.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                notificationGUI notificationGUI = new notificationGUI(); // Créer l'interface de validation admin
                notificationGUI.setVisible(true); // Afficher l'interface de validation admin
                dispose(); // Fermer l'interface actuelle
            }
        });

        btnVisualiserHistorique.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UserSearchGUI userSearchGUI = new UserSearchGUI(); // Créer l'interface de recherche utilisateur
                userSearchGUI.setVisible(true); // Afficher l'interface de recherche utilisateur
                dispose(); // Fermer l'interface actuelle
            }
        });

        btnGestionComptes.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new ConnexionGUI(); // Afficher l'interface de connexion
            }
        });

        btnRechercheClubs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String commune = (String) communeComboBox.getSelectedItem();
                String region = (String) regionComboBox.getSelectedItem();
                String departement = (String) departementComboBox.getSelectedItem();
                String federation = (String) federationComboBox.getSelectedItem();

                if (clubDAO != null) {
                    try {
                        if (commune != null && !commune.equals("Sélectionnez une commune") && federation != null && !federation.isEmpty()) {
                            String clubsInFederation = clubDAO.getClubsInFederationByCommune(federation, commune, null);
                            JOptionPane.showMessageDialog(PageAccueilGUI.this, "Nombre total de clubs dans la commune " + commune + " pour la fédération " + federation + " : " + clubsInFederation);
                        } else if (region != null && !region.isEmpty() && federation != null && !federation.isEmpty()) {
                            String clubsInFederation = clubDAO.getClubsInFederationByCommune(federation, null, region);
                            JOptionPane.showMessageDialog(PageAccueilGUI.this, "Nombre total de clubs dans la région " + region + " pour la fédération " + federation + " : " + clubsInFederation);
                        } else {
                            JOptionPane.showMessageDialog(PageAccueilGUI.this, "Veuillez sélectionner une commune et une fédération, ou une région et une fédération.");
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(PageAccueilGUI.this, "Erreur: " + ex.getMessage());
                    }
                } else {
                    JOptionPane.showMessageDialog(PageAccueilGUI.this, "Erreur: clubDAO est null.");
                }
            }
        });

        btnRechercheLicencies.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String commu = (String) communeComboBox.getSelectedItem();
                String fede = (String) federationComboBox.getSelectedItem();
                String reg = (String) regionComboBox.getSelectedItem();

                if ((fede != null && !fede.isEmpty()) && (commu != null && !commu.equals("Sélectionnez une commune"))) {
                    searchLicenciesByFederationAndCommune(fede, commu);
                }

                if ((fede != null && !fede.isEmpty()) && (reg != null && !reg.isEmpty())) {
                    if (!reg.equals("Sélectionnez une région")) {
                        searchLicenciesByRegionAndFederation(reg, fede);
                    }
                }

                if (fede == null || fede.isEmpty()) {
                    JOptionPane.showMessageDialog(PageAccueilGUI.this, "Veuillez sélectionner une fédération.");
                }

                if ((reg == null || reg.isEmpty()) && ("Sélectionnez une commune".equals(commu))) {
                    commu = null;
                }

                if ((reg == null || reg.isEmpty()) && (commu == null || commu.isEmpty())) {
                    JOptionPane.showMessageDialog(PageAccueilGUI.this, "Veuillez sélectionner une région ou une commune.");
                }
            }
        });

        // Ajout du panneau principal à la fenêtre
        add(panel);
        pack(); // Ajuste la taille de la fenêtre en fonction de ses composants
        setLocationRelativeTo(null); // Centre la fenêtre sur l'écran
        setVisible(true); // Rend la fenêtre visible
    }

    // Méthode pour créer des boutons avec un style personnalisé
    private JButton createStyledButton(String text) {
        JButton button = new JButton(text);
        button.setFont(mainFont);
        button.setBackground(Color.GRAY);
        button.setForeground(Color.WHITE);
        button.setFocusPainted(false);
        button.setBorder(null);
        button.setOpaque(true);
        button.setPreferredSize(new Dimension(300, 50));
        button.setCursor(new Cursor(Cursor.HAND_CURSOR));
        return button;
    }

    // Recherche des licenciés par région et fédération et affichage des résultats
    private void searchLicenciesByRegionAndFederation(String region, String federation) {
        int totalMaleLicencies = dao1.getTotalMaleLicenciesByRegionAndFederation(region, federation);
        int totalFemaleLicencies = dao1.getTotalFemaleLicenciesByRegionAndFederation(region, federation);
        JOptionPane.showMessageDialog(this, "Nombre total de licenciés hommes dans la région " + region + " et la fédération " + federation + " : " + totalMaleLicencies);
        JOptionPane.showMessageDialog(this, "Nombre total de licenciés femmes dans la région " + region + " et la fédération " + federation + " : " + totalFemaleLicencies);
    }

    // Recherche des licenciés par fédération et commune et affichage des résultats
    private void searchLicenciesByFederationAndCommune(String federation, String commune) {
        int totalMaleLicencies = dao1.getTotalMaleLicenciesByLocation(federation, commune);
        int totalFemaleLicencies = dao1.getTotalFemaleLicenciesByLocation(federation, commune);
        JOptionPane.showMessageDialog(this, "Nombre total de licenciés hommes dans la fédération " + federation + " et la commune " + commune + " : " + totalMaleLicencies);
        JOptionPane.showMessageDialog(this, "Nombre total de licenciés femmes dans la fédération " + federation + " et la commune " + commune + " : " + totalFemaleLicencies);
    }

    // Configuration de l'auto-complétion pour les JComboBox
    private void setupAutoComplete(JComboBox<String> comboBox, List<String> items) {
        JTextField textField = (JTextField) comboBox.getEditor().getEditorComponent();
        DefaultComboBoxModel<String> model = new DefaultComboBoxModel<>(items.toArray(new String[0]));
        comboBox.setModel(model);

        // Variable pour éviter les mises à jour continues
        final boolean[] updating = {false};

        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSuggestions();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSuggestions();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateSuggestions();
            }

            private void updateSuggestions() {
                if (updating[0]) return; // Éviter les mises à jour continues

                SwingUtilities.invokeLater(() -> {
                    String input = textField.getText();
                    if (input.isEmpty()) {
                        comboBox.hidePopup();
                        return;
                    }
                    List<String> suggestions = getSuggestions(input, items);
                    if (suggestions.isEmpty()) {
                        comboBox.hidePopup();
                    } else {
                        updating[0] = true;
                        DefaultComboBoxModel<String> newModel = new DefaultComboBoxModel<>(suggestions.toArray(new String[0]));
                        comboBox.setModel(newModel);
                        textField.setText(input);
                        comboBox.showPopup();
                        updating[0] = false;
                    }
                });
            }
        });

        comboBox.addActionListener(e -> {
            if (updating[0]) return;
            SwingUtilities.invokeLater(() -> {
                updating[0] = true;
                comboBox.hidePopup();
                updating[0] = false;
            });
        });
    }

    // Récupère les suggestions basées sur l'entrée de l'utilisateur
    private List<String> getSuggestions(String input, List<String> items) {
        List<String> suggestions = new ArrayList<>();
        for (String item : items) {
            if (item.toLowerCase().startsWith(input.toLowerCase())) {
                suggestions.add(item);
            }
        }
        return suggestions;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new PageAccueilGUI(); // Lance l'interface de la page d'accueil
            }
        });
    }
}
