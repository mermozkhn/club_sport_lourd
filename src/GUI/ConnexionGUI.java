package GUI;

import DAO.UtilisateurDAO;
import Model.Utilisateur;

import java.awt.EventQueue;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class ConnexionGUI {

    // Declaring GUI components and user data
    private JFrame frame;
    private JPasswordField passwordField;
    private JTextField textField;
    private JLabel lblErreur;
    private Utilisateur authenticatedUser;

    /**
     * Launch the application.
     * The main method to start the GUI application.
     */
    public static void main(String[] args) {
        // Using EventQueue to ensure thread safety in GUI operations
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    // Create an instance of ConnexionGUI to initialize and show the GUI
                    ConnexionGUI window = new ConnexionGUI();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     * Constructor that initializes the application.
     */
    public ConnexionGUI() {
        // Method to set up the initial state of the GUI
        initialize();
        // Make the frame visible
        this.frame.setVisible(true);
    }

    /**
     * Initialize the contents of the frame.
     * Method to define and arrange GUI components within the frame.
     */
    void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 476, 413); // Set the size and position of the frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close the application when the frame is closed
        lblErreur = new JLabel("");
        lblErreur.setForeground(Color.RED); // Set the error label text color to red
        lblErreur.setBounds(163, 97, 152, 17);
        lblErreur.setFont(new Font("Lucida Grande", Font.PLAIN, 11));
        frame.getContentPane().setLayout(null); // Use absolute layout for the frame

        lblErreur.setBackground(new Color(255, 19, 0));
        frame.getContentPane().add(lblErreur); // Add the error label to the frame

        JLabel lblNewLabel_1 = new JLabel("Identifiant");
        lblNewLabel_1.setForeground(Color.WHITE); // Set the text color to white
        lblNewLabel_1.setBounds(121, 126, 91, 16);
        frame.getContentPane().add(lblNewLabel_1);

        JButton btnNewButton = new JButton("Se connecter");
        btnNewButton.setBounds(157, 270, 107, 21); // Set the position and size of the button

        // Add action listener to handle button click event
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String identifiant = textField.getText();
                String enteredPassword = new String(passwordField.getPassword());

                // Authenticate the user
                UtilisateurDAO userDAO = new UtilisateurDAO();
                authenticatedUser = userDAO.userConnection(identifiant, enteredPassword);

                if (authenticatedUser != null) {
                    // Show the security question dialog if authentication is successful
                    showSecurityQuestionDialog(authenticatedUser.getEmail());
                    if (SystemTray.isSupported()) {
                        // Display system tray notification if supported
                        SystemTray tray = SystemTray.getSystemTray();
                        Image icon = Toolkit.getDefaultToolkit().getImage("icon.png");

                        // Load the notification image
                        Image notificationImage = Toolkit.getDefaultToolkit().getImage("notification.png");

                        // Create a popup menu for the system tray icon
                        PopupMenu popup = new PopupMenu();

                        // Create a tray icon with the loaded image
                        TrayIcon trayIcon = new TrayIcon(icon, "System Tray");
                        trayIcon.setImageAutoSize(true);
                        trayIcon.setPopupMenu(popup);

                        try {
                            tray.add(trayIcon);
                            // Display a notification message
                            trayIcon.displayMessage("echec de connection", "Vous n'êtes pas connecté.", TrayIcon.MessageType.INFO);
                        } catch (AWTException ex) {
                            System.err.println("TrayIcon could not be added.");
                        }
                    } else {
                        System.err.println("System Tray is not supported.");
                    }
                } else {
                    // Display an error message if authentication fails
                    lblErreur.setText("Erreur login ou password");
                }
            }
        });

        btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 10));
        btnNewButton.setForeground(new Color(255, 0, 0));
        frame.getContentPane().add(btnNewButton); // Add the button to the frame

        JLabel lblNewLabel_2 = new JLabel("Mot de passe");
        lblNewLabel_2.setForeground(Color.WHITE);
        lblNewLabel_2.setBounds(114, 174, 109, 16);
        frame.getContentPane().add(lblNewLabel_2);

        passwordField = new JPasswordField();
        passwordField.setBounds(215, 171, 152, 22);
        frame.getContentPane().add(passwordField); // Add the password field to the frame

        textField = new JTextField();
        textField.setBounds(215, 126, 152, 22);
        frame.getContentPane().add(textField); // Add the text field to the frame
        textField.setColumns(10);

        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setBounds(6, -5, 427, 119);
        lblNewLabel.setIcon(new ImageIcon("/Users/melly-anna/Downloads/Logo_ESIGELEC.svg-2.png")); // Set the icon for the label
        frame.getContentPane().add(lblNewLabel);

        JLabel lblNewLabel_3 = new JLabel("");
        lblNewLabel_3.setIcon(new ImageIcon("/Users/melly-anna/Downloads/B9729082724Z.1_20211122190149_000+GCVJCSVNJ.2-0.jpg")); // Set the icon for the label
        lblNewLabel_3.setBounds(-175, -29, 649, 407);
        frame.getContentPane().add(lblNewLabel_3);
    }

    /**
     * Show the security question dialog
     * Method to display a dialog with a security question for additional authentication.
     */
    private void showSecurityQuestionDialog(String adminEmail) {
        JDialog dialog = new JDialog(frame, "Question de sécurité", true);
        dialog.setLayout(null);
        dialog.setBounds(150, 150, 400, 200); // Set the size and position of the dialog

        JLabel questionLabel = new JLabel("Qui est le premier Admin?");
        questionLabel.setBounds(50, 30, 300, 25); // Set the position and size of the label
        dialog.add(questionLabel);

        JTextField answerField = new JTextField();
        answerField.setBounds(50, 70, 300, 25); // Set the position and size of the text field
        dialog.add(answerField);

        JButton submitButton = new JButton("Soumettre");
        submitButton.setBounds(150, 120, 100, 25); // Set the position and size of the button
        dialog.add(submitButton);

        // Add action listener to handle button click event
        submitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String answer = answerField.getText();
                if (validateSecurityAnswer(answer)) {
                    dialog.dispose(); // Close the dialog if the answer is correct
                    // Show the admin profile page with the provided email
                    new ProfilAdminGUI(adminEmail);
                    frame.dispose(); // Close the current frame
                } else {
                    // Show an error message if the answer is incorrect
                    JOptionPane.showMessageDialog(dialog, "Réponse incorrecte.", "Erreur", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        dialog.setVisible(true); // Make the dialog visible
    }

    /**
     * Validate the security answer
     * Method to validate the answer to the security question.
     */
    private boolean validateSecurityAnswer(String answer) {
        // Check if the answer matches the expected answer
        return "Gilles".equalsIgnoreCase(answer);
    }
}
