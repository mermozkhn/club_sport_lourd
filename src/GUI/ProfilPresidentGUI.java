package GUI;
import javax.swing.*;
import java.awt.*;


//Importations et déclaration de la classe principale continuent ici


public class ProfilPresidentGUI {
    private JFrame frame;
    private JLabel label;

    public ProfilPresidentGUI() {
        initialize();
        frame.setVisible(true);
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        frame.getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(new BorderLayout());

        label = new JLabel("Page Profil Président");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(label, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                ProfilPresidentGUI window = new ProfilPresidentGUI();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
