package GUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.*;
import org.mindrot.jbcrypt.BCrypt;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ProfilAdminGUI {
    private JFrame frame;
    private JTable table;
    private DefaultTableModel model;
    private JButton btnModify;
    private JComboBox<String> comboBox;
    private JTextField searchField;

    private String url = "jdbc:mysql://localhost:3306/club_sport";
    private String user = "root";
    private String password = "root";
    private String adminEmail; // Email de l'administrateur connecté

    public ProfilAdminGUI(String adminEmail) {
        this.adminEmail = adminEmail;
        initialize();
        frame.setVisible(true);
        addComboBox();
        addSearchField();
        populateTableFromDatabase();
        addModifyButton();
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(10, 10, 10, 10);

        searchField = new JTextField(20);
        JPanel searchPanel = new JPanel();
        searchPanel.add(new JLabel("Rechercher par nom : "));
        searchPanel.add(searchField);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        frame.getContentPane().add(searchPanel, gbc);

        gbc.gridy = 1;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;

        model = new DefaultTableModel(new String[]{"ID", "Nom", "Prénom", "Email", "Mot de passe", "Profil", "Nom du Fichier", "Domaine", "Fédération", "Valider"}, 0);
        table = new JTable(model);
        JScrollPane scrollPane = new JScrollPane(table);

        frame.getContentPane().add(scrollPane, gbc);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        JButton btnAdd = new JButton("Ajouter");
        btnAdd.addActionListener(e -> addRow());
        buttonPanel.add(btnAdd);

        JButton btnDelete = new JButton("Supprimer");
        btnDelete.addActionListener(e -> deleteRow());
        buttonPanel.add(btnDelete);

        btnModify = new JButton("Modifier");
        btnModify.addActionListener(e -> modifyRow());
        buttonPanel.add(btnModify);

        gbc.gridy = 2;
        gbc.weightx = 0;
        gbc.weighty = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        frame.getContentPane().add(buttonPanel, gbc);
    }

    private void populateTableFromDatabase() {
        String selectedOption = (String) comboBox.getSelectedItem();
        String query = "SELECT idusers, nom, prenom, email, motdepass, profil, fileName, domaine, fede, validate FROM users";

        if (selectedOption != null && !selectedOption.equals("Tous")) {
            query += " WHERE profil = '" + selectedOption + "'";
        }

        String searchName = searchField.getText().trim();
        if (!searchName.isEmpty()) {
            if (query.contains("WHERE")) {
                query += " AND nom LIKE '%" + searchName + "%'";
            } else {
                query += " WHERE nom LIKE '%" + searchName + "%'";
            }
        }

        try (Connection connection = DriverManager.getConnection(url, user, password);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {

            model.setRowCount(0);

            while (resultSet.next()) {
                int id = resultSet.getInt("idusers");
                String nom = resultSet.getString("nom");
                String prenom = resultSet.getString("prenom");
                String email = resultSet.getString("email");
                String motDePasse = resultSet.getString("motdepass");
                String profil = resultSet.getString("profil");
                String fileName = resultSet.getString("fileName");
                String domaine = resultSet.getString("domaine");
                String fede = resultSet.getString("fede");
                String validate = resultSet.getString("validate");
                model.addRow(new Object[]{id, nom, prenom, email, motDePasse, profil, fileName, domaine, fede, validate});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void addRow() {
        JFrame addUserFrame = new JFrame("Ajouter un utilisateur");
        addUserFrame.setBounds(200, 200, 400, 400);
        addUserFrame.setLayout(new GridLayout(10, 2, 5, 5));

        JTextField nomField = new JTextField();
        JTextField prenomField = new JTextField();
        JTextField emailField = new JTextField();
        JPasswordField passwordField = new JPasswordField();
        JComboBox<String> profilComboBox = new JComboBox<>();
        JTextField fileNameField = new JTextField();
        JButton fileChooserButton = new JButton("Choisir un fichier");
        JTextField domaineField = new JTextField();
        JTextField fedeField = new JTextField();
        JTextField validateField = new JTextField();

        profilComboBox.addItem("admin");
        profilComboBox.addItem("Président");
        profilComboBox.addItem("Sportif");

        final File[] selectedFile = new File[1];

        fileChooserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    selectedFile[0] = fileChooser.getSelectedFile();
                    fileNameField.setText(selectedFile[0].getName());
                }
            }
        });

        addUserFrame.add(new JLabel("Nom:"));
        addUserFrame.add(nomField);
        addUserFrame.add(new JLabel("Prénom:"));
        addUserFrame.add(prenomField);
        addUserFrame.add(new JLabel("Email:"));
        addUserFrame.add(emailField);
        addUserFrame.add(new JLabel("Mot de passe:"));
        addUserFrame.add(passwordField);
        addUserFrame.add(new JLabel("Profil:"));
        addUserFrame.add(profilComboBox);
        addUserFrame.add(new JLabel("Nom du fichier:"));
        addUserFrame.add(fileNameField);
        addUserFrame.add(new JLabel(""));
        addUserFrame.add(fileChooserButton);
        addUserFrame.add(new JLabel("Domaine:"));
        addUserFrame.add(domaineField);
        addUserFrame.add(new JLabel("Fédération:"));
        addUserFrame.add(fedeField);
        addUserFrame.add(new JLabel("Valider:"));
        addUserFrame.add(validateField);

        JButton btnAdd = new JButton("Ajouter");
        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String newNom = nomField.getText().trim();
                String newPrenom = prenomField.getText().trim();
                String newEmail = emailField.getText().trim();
                String newMotDePasse = new String(passwordField.getPassword());
                String newProfil = (String) profilComboBox.getSelectedItem();
                String newFileName = fileNameField.getText().trim();
                String newDomaine = domaineField.getText().trim();
                String newFede = fedeField.getText().trim();
                String newValidate = validateField.getText().trim();
                String hashedPassword = BCrypt.hashpw(newMotDePasse, BCrypt.gensalt());

                InputStream fileContent = null;
                try {
                    if (selectedFile[0] != null) {
                        fileContent = new FileInputStream(selectedFile[0]);
                    }
                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }

                model.addRow(new Object[]{"", newNom, newPrenom, newEmail, hashedPassword, newProfil, newFileName, newDomaine, newFede, newValidate});

                String query = "INSERT INTO users (nom, prenom, email, motdepass, profil, fileName, file, domaine, fede, validate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                try (Connection connection = DriverManager.getConnection(url, user, password);
                     PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
                    statement.setString(1, newNom);
                    statement.setString(2, newPrenom);
                    statement.setString(3, newEmail);
                    statement.setString(4, hashedPassword);
                    statement.setString(5, newProfil);
                    statement.setString(6, newFileName);
                    if (fileContent != null) {
                        statement.setBlob(7, fileContent);
                    } else {
                        statement.setNull(7, Types.BLOB);
                    }
                    statement.setString(8, newDomaine);
                    statement.setString(9, newFede);
                    statement.setString(10, newValidate);
                    statement.executeUpdate();

                    ResultSet generatedKeys = statement.getGeneratedKeys();
                    if (generatedKeys.next()) {
                        int id = generatedKeys.getInt(1);
                        int lastRow = model.getRowCount() - 1;
                        model.setValueAt(id, lastRow, 0);
                    }

                    logAction("Ajout d'utilisateur: " + newNom + " " + newPrenom, adminEmail);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }

                addUserFrame.dispose();
            }
        });
        addUserFrame.add(btnAdd);

        JButton btnCancel = new JButton("Annuler");
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addUserFrame.dispose();
            }
        });
        addUserFrame.add(btnCancel);

        addUserFrame.setVisible(true);
    }

    private void deleteRow() {
        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            int idToDelete = (int) model.getValueAt(selectedRow, 0);
            String nom = (String) model.getValueAt(selectedRow, 1);
            String prenom = (String) model.getValueAt(selectedRow, 2);

            model.removeRow(selectedRow);

            String query = "DELETE FROM users WHERE idusers = ?";
            try (Connection connection = DriverManager.getConnection(url, user, password);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setInt(1, idToDelete);
                statement.executeUpdate();
                logAction("Suppression d'utilisateur: " + nom + " " + prenom, adminEmail);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void modifyRow() {
        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            int idToUpdate = (int) model.getValueAt(selectedRow, 0);
            String oldNom = (String) model.getValueAt(selectedRow, 1);
            String oldPrenom = (String) model.getValueAt(selectedRow, 2);

            String newNom = JOptionPane.showInputDialog(frame, "Nouveau nom:", oldNom);
            String newPrenom = JOptionPane.showInputDialog(frame, "Nouveau prénom:", oldPrenom);
            String newEmail = JOptionPane.showInputDialog(frame, "Nouvel email:", model.getValueAt(selectedRow, 3));
            String newMotDePasse = JOptionPane.showInputDialog(frame, "Nouveau mot de passe:");
            String newProfil = JOptionPane.showInputDialog(frame, "Nouveau profil:", model.getValueAt(selectedRow, 5));
            String newFileName = JOptionPane.showInputDialog(frame, "Nouveau nom du fichier:", model.getValueAt(selectedRow, 6));
            String newDomaine = JOptionPane.showInputDialog(frame, "Nouveau domaine:", model.getValueAt(selectedRow, 7));
            String newFede = JOptionPane.showInputDialog(frame, "Nouvelle fédération:", model.getValueAt(selectedRow, 8));
            String newValidate = JOptionPane.showInputDialog(frame, "Nouvelle validation:", model.getValueAt(selectedRow, 9));

            String hashedPassword = BCrypt.hashpw(newMotDePasse, BCrypt.gensalt());

            model.setValueAt(newNom, selectedRow, 1);
            model.setValueAt(newPrenom, selectedRow, 2);
            model.setValueAt(newEmail, selectedRow, 3);
            model.setValueAt(hashedPassword, selectedRow, 4);
            model.setValueAt(newProfil, selectedRow, 5);
            model.setValueAt(newFileName, selectedRow, 6);
            model.setValueAt(newDomaine, selectedRow, 7);
            model.setValueAt(newFede, selectedRow, 8);
            model.setValueAt(newValidate, selectedRow, 9);

            String query = "UPDATE users SET nom = ?, prenom = ?, email = ?, motdepass = ?, profil = ?, fileName = ?, domaine = ?, fede = ?, validate = ? WHERE idusers = ?";
            try (Connection connection = DriverManager.getConnection(url, user, password);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.setString(1, newNom);
                statement.setString(2, newPrenom);
                statement.setString(3, newEmail);
                statement.setString(4, hashedPassword);
                statement.setString(5, newProfil);
                statement.setString(6, newFileName);
                statement.setString(7, newDomaine);
                statement.setString(8, newFede);
                statement.setString(9, newValidate);
                statement.setInt(10, idToUpdate);
                statement.executeUpdate();
                logAction("Modification d'utilisateur: " + oldNom + " " + oldPrenom + " à " + newNom + " " + newPrenom, adminEmail);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void addModifyButton() {
        btnModify = new JButton("Modifier");
        btnModify.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modifyRow();
            }
        });

        JPanel buttonPanel = (JPanel) frame.getContentPane().getComponent(2);
        buttonPanel.add(btnModify);
    }

    private void addComboBox() {
        comboBox = new JComboBox<>();
        comboBox.addItem("Tous");
        comboBox.addItem("admin");
        comboBox.addItem("Président");
        comboBox.addItem("Sportif");
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                populateTableFromDatabase();
            }
        });

        JPanel comboPanel = new JPanel();
        comboPanel.add(new JLabel("Filtrer par profil : "));
        comboPanel.add(comboBox);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        frame.getContentPane().add(comboPanel, gbc);
    }

    private void addSearchField() {
        searchField = new JTextField(20);
        searchField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                populateTableFromDatabase();
            }
        });

        JPanel searchPanel = new JPanel();
        searchPanel.add(new JLabel("Rechercher par nom : "));
        searchPanel.add(searchField);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 2;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        frame.getContentPane().add(searchPanel, gbc);
    }

    private void logAction(String action, String adminEmail) {
        String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        String logEntry = String.format("%s - Admin: %s - %s", timestamp, adminEmail, action);
        
        File file = new File("admin.txt");
        try (FileWriter fw = new FileWriter(file, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(logEntry);
            System.out.println("Action logged in: " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println("Current working directory: " + System.getProperty("user.dir"));
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ProfilAdminGUI window = new ProfilAdminGUI("admin@example.com"); // Exemple d'email
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
