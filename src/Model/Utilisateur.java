package Model;
import java.io.InputStream;

import org.mindrot.jbcrypt.BCrypt;

public class Utilisateur {
    private int idusers;
    private String nom;
    private String prenom;
    private String email;
    private String motdepass;
    private String profil;
    private String fileName;
    private InputStream file;
    private String domaine;
    private String fede;
    private String validate;

    public Utilisateur(int idusers, String nom, String prenom, String email, String motdepass, String profil) {
        this.idusers = idusers;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.motdepass = motdepass;
        this.profil = profil;
    }

    public int getIdusers() {
        return idusers;
    }

    public void setIdusers(int idusers) {
        this.idusers = idusers;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMotdepass() {
        return motdepass;
    }

    public void setMotdepass(String motdepass) {
        // Hasher le mot de passe avec BCrypt avant de l'affecter
        String hashedPassword = BCrypt.hashpw(motdepass, BCrypt.gensalt());
        this.motdepass = hashedPassword;
    }

    public boolean checkPassword(String plainPassword) {
        // Vérifier si le mot de passe correspond au hash stocké
        return BCrypt.checkpw(plainPassword, this.motdepass);
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public InputStream getFile() {
        return file;
    }

    public void setFile(InputStream file) {
        this.file = file;
    }

    public String getDomaine() {
        return domaine;
    }

    public void setDomaine(String domaine) {
        this.domaine = domaine;
    }

    public String getFede() {
        return fede;
    }

    public void setFede(String fede) {
        this.fede = fede;
    }

    public String getValidate() {
        return validate;
    }

    public void setValidate(String validate) {
        this.validate = validate;
    }
}