package Model; 

 

public class Licencie{ 

  

 private String Code_commune; 

 private String Commune; 

 private String Departement; 

 private String Region; 

 private String Statut_geo; 

 private String Code; 

 private String Federation; 

 private String F_1_4_ans; 

 private String F_5_9_ans; 

 private String F_10_14_ans; 

 private String F_15_19_ans; 

 private String F_20_24_ans; 

 private String F_25_29_ans; 

 private String F_30_34_ans; 

 private String F_35_39_ans; 

 private String F_40_44_ans; 

 private String F_45_49_ans; 

 private String F_50_54_ans; 

 private String F_55_59_ans; 

 private String F_60_64_ans; 

 private String F_65_69_ans; 

 private String F_70_74_ans; 

 private String F_75_79_ans; 

 private String F_80_99_ans; 

 private String F_NR; 

 private String H_1_4_ans; 

 private String H_5_9_ans; 

 private String H_10_14_ans; 

 private String H_15_19_ans; 

 private String H_20_24_ans; 

 private String H_25_29_ans; 

 private String H_30_34_ans; 

 private String H_35_39_ans; 

 private String H_40_44_ans; 

 private String H_45_49_ans; 

 private String H_50_54_ans; 

 private String H_55_59_ans; 

 private String H_60_64_ans; 

 private String H_65_69_ans; 

 private String H_70_74_ans; 

 private String H_75_79_ans; 

 private String H_80_99_ans; 

 private String H_NR; 

 private String NR_NR; 

 private String Total; 

/** 

 * @return the code_commune 

 */ 

public String getCode_commune() { 

return Code_commune; 

} 

/** 

 * @param code_commune the code_commune to set 

 */ 

public void setCode_commune(String code_commune) { 

Code_commune = code_commune; 

} 

/** 

 * @return the commune 

 */ 

public String getCommune() { 

return Commune; 

} 

/** 

 * @param commune the commune to set 

 */ 

public void setCommune(String commune) { 

Commune = commune; 

} 

/** 

 * @return the departement 

 */ 

public String getDepartement() { 

return Departement; 

} 

/** 

 * @param departement the departement to set 

 */ 

public void setDepartement(String departement) { 

Departement = departement; 

} 

/** 

 * @return the region 

 */ 

public String getRegion() { 

return Region; 

} 

/** 

 * @param region the region to set 

 */ 

public void setRegion(String region) { 

Region = region; 

} 

/** 

 * @return the statut_geo 

 */ 

public String getStatut_geo() { 

return Statut_geo; 

} 

/** 

 * @param statut_geo the statut_geo to set 

 */ 

public void setStatut_geo(String statut_geo) { 

Statut_geo = statut_geo; 

} 

/** 

 * @return the code 

 */ 

public String getCode() { 

return Code; 

} 

/** 

 * @param code the code to set 

 */ 

public void setCode(String code) { 

Code = code; 

} 

/** 

 * @return the federation 

 */ 

public String getFederation() { 

return Federation; 

} 

/** 

 * @param federation the federation to set 

 */ 

public void setFederation(String federation) { 

Federation = federation; 

} 

/** 

 * @return the f_1_4_ans 

 */ 

public String getF_1_4_ans() { 

return F_1_4_ans; 

} 

/** 

 * @param f_1_4_ans the f_1_4_ans to set 

 */ 

public void setF_1_4_ans(String f_1_4_ans) { 

F_1_4_ans = f_1_4_ans; 

} 

/** 

 * @return the f_5_9_ans 

 */ 

public String getF_5_9_ans() { 

return F_5_9_ans; 

} 

/** 

 * @param f_5_9_ans the f_5_9_ans to set 

 */ 

public void setF_5_9_ans(String f_5_9_ans) { 

F_5_9_ans = f_5_9_ans; 

} 

/** 

 * @return the f_10_14_ans 

 */ 

public String getF_10_14_ans() { 

return F_10_14_ans; 

} 

/** 

 * @param f_10_14_ans the f_10_14_ans to set 

 */ 

public void setF_10_14_ans(String f_10_14_ans) { 

F_10_14_ans = f_10_14_ans; 

} 

/** 

 * @return the f_15_19_ans 

 */ 

public String getF_15_19_ans() { 

return F_15_19_ans; 

} 

/** 

 * @param f_15_19_ans the f_15_19_ans to set 

 */ 

public void setF_15_19_ans(String f_15_19_ans) { 

F_15_19_ans = f_15_19_ans; 

} 

/** 

 * @return the f_20_24_ans 

 */ 

public String getF_20_24_ans() { 

return F_20_24_ans; 

} 

/** 

 * @param f_20_24_ans the f_20_24_ans to set 

 */ 

public void setF_20_24_ans(String f_20_24_ans) { 

F_20_24_ans = f_20_24_ans; 

} 

/** 

 * @return the f_25_29_ans 

 */ 

public String getF_25_29_ans() { 

return F_25_29_ans; 

} 

/** 

 * @param f_25_29_ans the f_25_29_ans to set 

 */ 

public void setF_25_29_ans(String f_25_29_ans) { 

F_25_29_ans = f_25_29_ans; 

} 

/** 

 * @return the f_30_34_ans 

 */ 

public String getF_30_34_ans() { 

return F_30_34_ans; 

} 

/** 

 * @param f_30_34_ans the f_30_34_ans to set 

 */ 

public void setF_30_34_ans(String f_30_34_ans) { 

F_30_34_ans = f_30_34_ans; 

} 

/** 

 * @return the f_35_39_ans 

 */ 

public String getF_35_39_ans() { 

return F_35_39_ans; 

} 

/** 

 * @param f_35_39_ans the f_35_39_ans to set 

 */ 

public void setF_35_39_ans(String f_35_39_ans) { 

F_35_39_ans = f_35_39_ans; 

} 

/** 

 * @return the f_40_44_ans 

 */ 

public String getF_40_44_ans() { 

return F_40_44_ans; 

} 

/** 

 * @param f_40_44_ans the f_40_44_ans to set 

 */ 

public void setF_40_44_ans(String f_40_44_ans) { 

F_40_44_ans = f_40_44_ans; 

} 

/** 

 * @return the f_45_49_ans 

 */ 

public String getF_45_49_ans() { 

return F_45_49_ans; 

} 

/** 

 * @param f_45_49_ans the f_45_49_ans to set 

 */ 

public void setF_45_49_ans(String f_45_49_ans) { 

F_45_49_ans = f_45_49_ans; 

} 

/** 

 * @return the f_50_54_ans 

 */ 

public String getF_50_54_ans() { 

return F_50_54_ans; 

} 

/** 

 * @param f_50_54_ans the f_50_54_ans to set 

 */ 

public void setF_50_54_ans(String f_50_54_ans) { 

F_50_54_ans = f_50_54_ans; 

} 

/** 

 * @return the f_55_59_ans 

 */ 

public String getF_55_59_ans() { 

return F_55_59_ans; 

} 

/** 

 * @param f_55_59_ans the f_55_59_ans to set 

 */ 

public void setF_55_59_ans(String f_55_59_ans) { 

F_55_59_ans = f_55_59_ans; 

} 

/** 

 * @return the f_60_64_ans 

 */ 

public String getF_60_64_ans() { 

return F_60_64_ans; 

} 

/** 

 * @param f_60_64_ans the f_60_64_ans to set 

 */ 

public void setF_60_64_ans(String f_60_64_ans) { 

F_60_64_ans = f_60_64_ans; 

} 

/** 

 * @return the f_65_69_ans 

 */ 

public String getF_65_69_ans() { 

return F_65_69_ans; 

} 

/** 

 * @param f_65_69_ans the f_65_69_ans to set 

 */ 

public void setF_65_69_ans(String f_65_69_ans) { 

F_65_69_ans = f_65_69_ans; 

} 

/** 

 * @return the f_70_74_ans 

 */ 

public String getF_70_74_ans() { 

return F_70_74_ans; 

} 

/** 

 * @param f_70_74_ans the f_70_74_ans to set 

 */ 

public void setF_70_74_ans(String f_70_74_ans) { 

F_70_74_ans = f_70_74_ans; 

} 

/** 

 * @return the f_75_79_ans 

 */ 

public String getF_75_79_ans() { 

return F_75_79_ans; 

} 

/** 

 * @param f_75_79_ans the f_75_79_ans to set 

 */ 

public void setF_75_79_ans(String f_75_79_ans) { 

F_75_79_ans = f_75_79_ans; 

} 

/** 

 * @return the f_80_99_ans 

 */ 

public String getF_80_99_ans() { 

return F_80_99_ans; 

} 

/** 

 * @param f_80_99_ans the f_80_99_ans to set 

 */ 

public void setF_80_99_ans(String f_80_99_ans) { 

F_80_99_ans = f_80_99_ans; 

} 

/** 

 * @return the f_NR 

 */ 

public String getF_NR() { 

return F_NR; 

} 

/** 

 * @param f_NR the f_NR to set 

 */ 

public void setF_NR(String f_NR) { 

F_NR = f_NR; 

} 

/** 

 * @return the h_1_4_ans 

 */ 

public String getH_1_4_ans() { 

return H_1_4_ans; 

} 

/** 

 * @param h_1_4_ans the h_1_4_ans to set 

 */ 

public void setH_1_4_ans(String h_1_4_ans) { 

H_1_4_ans = h_1_4_ans; 

} 

/** 

 * @return the h_5_9_ans 

 */ 

public String getH_5_9_ans() { 

return H_5_9_ans; 

} 

/** 

 * @param h_5_9_ans the h_5_9_ans to set 

 */ 

public void setH_5_9_ans(String h_5_9_ans) { 

H_5_9_ans = h_5_9_ans; 

} 

/** 

 * @return the h_10_14_ans 

 */ 

public String getH_10_14_ans() { 

return H_10_14_ans; 

} 

/** 

 * @param h_10_14_ans the h_10_14_ans to set 

 */ 

public void setH_10_14_ans(String h_10_14_ans) { 

H_10_14_ans = h_10_14_ans; 

} 

/** 

 * @return the h_15_19_ans 

 */ 

public String getH_15_19_ans() { 

return H_15_19_ans; 

} 

/** 

 * @param h_15_19_ans the h_15_19_ans to set 

 */ 

public void setH_15_19_ans(String h_15_19_ans) { 

H_15_19_ans = h_15_19_ans; 

} 

/** 

 * @return the h_20_24_ans 

 */ 

public String getH_20_24_ans() { 

return H_20_24_ans; 

} 

/** 

 * @param h_20_24_ans the h_20_24_ans to set 

 */ 

public void setH_20_24_ans(String h_20_24_ans) { 

H_20_24_ans = h_20_24_ans; 

} 

/** 

 * @return the h_25_29_ans 

 */ 

public String getH_25_29_ans() { 

return H_25_29_ans; 

} 

/** 

 * @param h_25_29_ans the h_25_29_ans to set 

 */ 

public void setH_25_29_ans(String h_25_29_ans) { 

H_25_29_ans = h_25_29_ans; 

} 

/** 

 * @return the h_30_34_ans 

 */ 

public String getH_30_34_ans() { 

return H_30_34_ans; 

} 

/** 

 * @param h_30_34_ans the h_30_34_ans to set 

 */ 

public void setH_30_34_ans(String h_30_34_ans) { 

H_30_34_ans = h_30_34_ans; 

} 

/** 

 * @return the h_35_39_ans 

 */ 

public String getH_35_39_ans() { 

return H_35_39_ans; 

} 

/** 

 * @param h_35_39_ans the h_35_39_ans to set 

 */ 

public void setH_35_39_ans(String h_35_39_ans) { 

H_35_39_ans = h_35_39_ans; 

} 

/** 

 * @return the h_40_44_ans 

 */ 

public String getH_40_44_ans() { 

return H_40_44_ans; 

} 

/** 

 * @param h_40_44_ans the h_40_44_ans to set 

 */ 

public void setH_40_44_ans(String h_40_44_ans) { 

H_40_44_ans = h_40_44_ans; 

} 

/** 

 * @return the h_45_49_ans 

 */ 

public String getH_45_49_ans() { 

return H_45_49_ans; 

} 

/** 

 * @param h_45_49_ans the h_45_49_ans to set 

 */ 

public void setH_45_49_ans(String h_45_49_ans) { 

H_45_49_ans = h_45_49_ans; 

} 

/** 

 * @return the h_50_54_ans 

 */ 

public String getH_50_54_ans() { 

return H_50_54_ans; 

} 

/** 

 * @param h_50_54_ans the h_50_54_ans to set 

 */ 

public void setH_50_54_ans(String h_50_54_ans) { 

H_50_54_ans = h_50_54_ans; 

} 

/** 

 * @return the h_55_59_ans 

 */ 

public String getH_55_59_ans() { 

return H_55_59_ans; 

} 

/** 

 * @param h_55_59_ans the h_55_59_ans to set 

 */ 

public void setH_55_59_ans(String h_55_59_ans) { 

H_55_59_ans = h_55_59_ans; 

} 

/** 

 * @return the h_60_64_ans 

 */ 

public String getH_60_64_ans() { 

return H_60_64_ans; 

} 

/** 

 * @param h_60_64_ans the h_60_64_ans to set 

 */ 

public void setH_60_64_ans(String h_60_64_ans) { 

H_60_64_ans = h_60_64_ans; 

} 

/** 

 * @return the h_65_69_ans 

 */ 

public String getH_65_69_ans() { 

return H_65_69_ans; 

} 

/** 

 * @param h_65_69_ans the h_65_69_ans to set 

 */ 

public void setH_65_69_ans(String h_65_69_ans) { 

H_65_69_ans = h_65_69_ans; 

} 

/** 

 * @return the _H_70_74_ans 

 */ 

public String getH_70_74_ans() { 

return H_70_74_ans; 

} 

/** 

 * @param _H_70_74_ans the _H_70_74_ans to set 

 */ 

public void setH_70_74_ans(String H_70_74_ans) { 

this.H_70_74_ans = H_70_74_ans; 

} 

/** 

 * @return the h_75_79_ans 

 */ 

public String getH_75_79_ans() { 

return H_75_79_ans; 

} 

/** 

 * @param h_75_79_ans the h_75_79_ans to set 

 */ 

public void setH_75_79_ans(String h_75_79_ans) { 

H_75_79_ans = h_75_79_ans; 

} 

/** 

 * @return the h_80_99_ans 

 */ 

public String getH_80_99_ans() { 

return H_80_99_ans; 

} 

/** 

 * @param h_80_99_ans the h_80_99_ans to set 

 */ 

public void setH_80_99_ans(String h_80_99_ans) { 

H_80_99_ans = h_80_99_ans; 

} 

/** 

 * @return the h_NR 

 */ 

public String getH_NR() { 

return H_NR; 

} 

/** 

 * @param h_NR the h_NR to set 

 */ 

public void setH_NR(String h_NR) { 

H_NR = h_NR; 

} 

/** 

 * @return the nR_NR 

 */ 

public String getNR_NR() { 

return NR_NR; 

} 

/** 

 * @param nR_NR the nR_NR to set 

 */ 

public void setNR_NR(String nR_NR) { 

NR_NR = nR_NR; 

} 

/** 

 * @return the total 

 */ 

public String getTotal() { 

return Total; 

} 

/** 

 * @param total the total to set 

 */ 

public void setTotal(String total) { 

Total = total; 

} 

  

} 