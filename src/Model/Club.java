package Model;
import java.util.ArrayList;


public class Club {
	private int id_club;
	private String Code_commune;
	private String Code_federation;
	private String Nom_federation;
	private int departement;
	private String region;
	private int Total_club;
	private ArrayList<Evenement> evenements;
	
	public Club(int id_club, String Code_commune,String Code_federation,String Nom_federation,int departement,String region,int Total_club) {
		this.id_club = id_club;
        this.Code_commune = Code_commune;
        this.Code_federation = Code_federation;
        this.Nom_federation = Nom_federation;
        this.departement = departement;
        this.region = region;
        this.Total_club = Total_club;
        this.evenements = new ArrayList<>();
	}
	public int getId_club() {
        return id_club;
    }

    public String getCode_commune() {
        return Code_commune;
    }

    public String getCode_federation() {
        return Code_federation;
    }

    public String getNom_federation() {
        return Nom_federation;
    }

    public int getDepartement() {
        return departement;
    }

    public String getRegion() {
        return region;
    }

    public int getTotal_club() {
        return Total_club;
    }

    // Setters
    public void setId_club(int id_club) {
        this.id_club = id_club;
    }

    public void setCode_commune(String Code_commune) {
        this.Code_commune = Code_commune;
    }

    public void setCode_federation(String Code_federation) {
        this.Code_federation = Code_federation;
    }

    public void setNom_federation(String Nom_federation) {
        this.Nom_federation = Nom_federation;
    }

    public void setDepartement(int departement) {
        this.departement = departement;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setTotal_club(int Total_club) {
        this.Total_club = Total_club;
    }
    public ArrayList<Evenement> getEvenements() {
        return evenements;
    }

    // Méthode setter pour définir la liste des événements
    public void setEvenements(ArrayList<Evenement> evenements) {
        this.evenements = evenements;
    }
	

}
