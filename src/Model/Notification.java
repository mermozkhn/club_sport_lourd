package Model;

public class Notification {
private int IDnotif ;
private String Titre;
private String Contenue;
public  Notification(int IDnotif,String Titre,String Contenue) {
this.IDnotif=IDnotif;
this.Titre=Titre;
this.Contenue=Contenue;

}
public int getIDnotif() {
return IDnotif;
}
public void setIDnotif(int IDnotif) {
this.IDnotif=IDnotif;
}
public String getTitre() {
return Titre;
}
public void setTitre(String Titre) {
this.Titre=Titre;
}
public String getContenue() {
return Contenue;
}
public void setContenue(String Contenue) {
this.Contenue=Contenue;
}
}
